//
//  Suscripcion.swift
//  Pharmaco
//
//  Created by Nicolas Caceres on 10/3/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation

class Suscripcion
{
    var id: String
    var cancelado: Bool
    var fechaInicio: Date
    var fechaFin: Date
    var fechaAgendada: Array<Date>
    var fechaEntrega: Array<Date>
    var QR:String
    var farmacia:Farmacia
    var medicamentos:[Medicamento]
    var ordenes:[OrdenMedica]
    
    init(id: String, cancelado: Bool, fechaInicio: Date, fechaFin: Date, fechaAngendada: Array<Date>, fechaEntrega: Array<Date>, QR: String, farmacia: Farmacia, medicamentos: [Medicamento], ordenes: [OrdenMedica]) {
        self.id = id
        self.cancelado = cancelado
        self.fechaInicio = fechaInicio
        self.fechaFin = fechaFin
        self.fechaAgendada = fechaAngendada
        self.fechaEntrega = fechaEntrega
        self.QR = QR
        self.farmacia = farmacia
        self.medicamentos = medicamentos
        self.ordenes = ordenes
    }
    //Constructor para el front
    init(fechaInicio: Date, fechaFin: Date, fechaAngendada: Array<Date>, QR: String, farmacia: Farmacia, medicamentos: [Medicamento], ordenes: [OrdenMedica]) {
        self.id = ""
        self.cancelado = false
        self.fechaInicio = fechaInicio
        self.fechaFin = fechaFin
        self.fechaAgendada = fechaAngendada
        self.fechaEntrega = Array<Date>()
        self.QR = QR
        self.farmacia = farmacia
        self.medicamentos = medicamentos
        self.ordenes = ordenes
    }
}


