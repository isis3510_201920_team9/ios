//
//  SuscripcionPersistence.swift
//  Pharmaco
//
//  Created by Nicolas Caceres on 10/3/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation
import Firebase

class SuscripcionPersistence
{
    static private let db = Firestore.firestore()
    
    static func getSuscripciones (completion: @escaping ([Suscripcion], Error?) -> Void) {
        var lista = [Suscripcion]()
        db.collection("/clientes/\(Autenticar.getUser().uid)/suscripciones").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                completion(lista, err)
                
            } else {
                for document in querySnapshot!.documents {
                    //print("\(document.documentID) => \(document.data())")
                    let medicamentosDict = document.get("medicamentos") as! Array<Dictionary<String,Any>>
                    let medicamentos = self.transformarMedicamentos(medicamentos: medicamentosDict)
                    let qr = document.get("qr") as! String
                    let farmaciaDict = document.get("farmacia") as! Dictionary<String,Any>
                    let farmacia = self.transformarFarmacia(farmacia: farmaciaDict)
                    let listaOrdenes = document.get("ordenes") as! Array<DocumentReference>
                    let fechaInicioSeg = document.get("fechaInicio") as! Timestamp
                    let fechaInicio = Date(timeIntervalSince1970: TimeInterval(fechaInicioSeg.seconds)-18000)
                    let fechaFinSeg = document.get("fechaFin") as! Timestamp
                    let fechaFin = Date(timeIntervalSince1970: TimeInterval(fechaFinSeg.seconds)-18000)
                    let intervalos = document.get("fechaAgendada") as! Array<Timestamp>
                    let fechaAgendada = self.transformarFechas(fechas: intervalos)
                    let entregas = document.get("fechaEntrega") as? Array<Timestamp>
                    let fechaEntrega: Array<Date>
                    if(entregas != nil)
                    {
                        fechaEntrega = self.transformarFechas(fechas: entregas!)
                    }
                    else
                    {
                        fechaEntrega = Array<Date>()
                    }
                    let cancelado = document.get("fechaEntrega") as? Bool
                    let canceladoFinal = cancelado ?? false
                    self.transformarOrdenes(ordenes: listaOrdenes){(ordenesTrans, error) in
                        let suscripcionActual = Suscripcion(id: document.documentID, cancelado: canceladoFinal, fechaInicio: fechaInicio, fechaFin: fechaFin, fechaAngendada: fechaAgendada, fechaEntrega: fechaEntrega, QR: qr, farmacia: farmacia, medicamentos: medicamentos, ordenes: ordenesTrans)
                        //print(suscripcionActual.ordenes)
                        lista.append(suscripcionActual)
                        if(lista.count == querySnapshot!.documents.count)
                        {
                            completion(lista, nil)
                        }
                    }
                }
            }
        }
    }
    
    static func getSuscripcionesVigentes (completion: @escaping ([Suscripcion], Error?) -> Void) {
        var lista = [Suscripcion]()
        db.collection("/clientes/\(Autenticar.getUser().uid)/suscripciones").whereField("cancelado", isEqualTo: false).order(by: "fechaInicio", descending: true).getDocuments()  { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                completion(lista, err)
                
            } else {
                for document in querySnapshot!.documents {
                    //print("\(document.documentID) => \(document.data())")
                    let medicamentosDict = document.get("medicamentos") as! Array<Dictionary<String,Any>>
                    let medicamentos = self.transformarMedicamentos(medicamentos: medicamentosDict)
                    let qr = document.get("qr") as! String
                    let farmaciaDict = document.get("farmacia") as! Dictionary<String,Any>
                    let farmacia = self.transformarFarmacia(farmacia: farmaciaDict)
                    let listaOrdenes = document.get("ordenes") as! Array<DocumentReference>
                    let fechaInicioSeg = document.get("fechaInicio") as! Timestamp
                    let fechaInicio = Date(timeIntervalSince1970: TimeInterval(fechaInicioSeg.seconds)-18000)
                    let fechaFinSeg = document.get("fechaFin") as! Timestamp
                    let fechaFin = Date(timeIntervalSince1970: TimeInterval(fechaFinSeg.seconds)-18000)
                    let intervalos = document.get("fechaAgendada") as! Array<Timestamp>
                    let fechaAgendada = self.transformarFechas(fechas: intervalos)
                    let entregas = document.get("fechaEntrega") as? Array<Timestamp>
                    let fechaEntrega: Array<Date>
                    if(entregas != nil)
                    {
                        fechaEntrega = self.transformarFechas(fechas: entregas!)
                    }
                    else
                    {
                        fechaEntrega = Array<Date>()
                    }
                    let cancelado = document.get("fechaEntrega") as? Bool
                    let canceladoFinal = cancelado ?? false
                    self.transformarOrdenes(ordenes: listaOrdenes){(ordenesTrans, error) in
                        let suscripcionActual = Suscripcion(id: document.documentID, cancelado: canceladoFinal, fechaInicio: fechaInicio, fechaFin: fechaFin, fechaAngendada: fechaAgendada, fechaEntrega: fechaEntrega, QR: qr, farmacia: farmacia, medicamentos: medicamentos, ordenes: ordenesTrans)
                        //print(suscripcionActual.ordenes)
                        lista.append(suscripcionActual)
                        if(lista.count == querySnapshot!.documents.count)
                        {
                            completion(lista, nil)
                        }
                    }
                }
            }
        }
    }
    
    static func transformarOrdenes(ordenes: Array<DocumentReference>, completion: @escaping (Array<OrdenMedica>, Error?) -> Void)
    {
        var lista = Array<OrdenMedica>()
        var contador = ordenes.count
        for orden in ordenes{
            orden.getDocument(){ (document, error) in
                if let document = document, document.exists {
                    contador-=1;
                    let doctor = document.get("doctor") as! String
                    let fechaSeg = document.get("fecha") as! Timestamp
                    let fecha = Date(timeIntervalSince1970: TimeInterval(fechaSeg.seconds)-18000)
                    let tipo = document.get("tipo") as! String
                    
                    let audioOpc = document.get("audio") as? String
                    let fotoOpc  = document.get("foto") as? String
                    let audio = audioOpc ?? ""
                    let foto = fotoOpc ?? ""
                    let idOrden = document.documentID
                    let especialidad = document.get("especialidad") as! String
                    lista.append(OrdenMedica(especialidad:especialidad, doctor: doctor, fecha: fecha, tipo: tipo, audio: audio, foto: foto, id: idOrden))
                    if(contador == 0)
                    {
                        completion(lista, nil)
                    }
                    
                } else {
                    print("Document does not exist")
                    completion(lista, nil)
                }
            }
        }
    }
    
    static func transformarFechas(fechas: Array<Timestamp>) -> Array<Date> {
        var lista = Array<Date>()
        for fecha in fechas {
            let fechaFormato = Date(timeIntervalSince1970: TimeInterval(fecha.seconds)-18000)
            lista.append(fechaFormato)
            
        }
        return lista
    }
    
    static func transformarMedicamentos(medicamentos: Array<Dictionary<String,Any>>) -> Array<Medicamento>
    {
        var lista = Array<Medicamento>()
        for medicamento in medicamentos
        {
            let nombre = medicamento["nombre"] as! String
            let nombreLab = medicamento["nombreLab"] as! String
            let dosisNumero = medicamento["dosisNumero"] as! Int
            let dosisUnidad = medicamento["dosisUnidad"] as! String
            let unidadesPedidas = medicamento["unidadesPedidas"] as! Int
            let dosisCaja = medicamento["dosisCaja"] as! Int
            lista.append(Medicamento(nombre: nombre, nombreLab: nombreLab, dosisNumero: dosisNumero, dosisUnidad: dosisUnidad, unidadesPedidas: unidadesPedidas, dosisCaja: dosisCaja))
        }
        return lista
        
    }
    static func transformarFarmacia(farmacia: Dictionary<String,Any>) -> Farmacia
    {
        let direccion = farmacia["direccion"] as! String
        let nombre = farmacia["nombre"] as! String
        let telefono = farmacia["telefono"] as! String
        return Farmacia(direccion: direccion, telefono: telefono, nombre: nombre)
        
    }
    static func postSuscripcion (suscripcion: Suscripcion,completion: @escaping (Bool, Error?) -> Void) {
        var ref: DocumentReference? = nil
        var datos = [String:Any?]()
        datos["qr"] = suscripcion.QR
        datos["fechaInicio"] = Date(timeIntervalSince1970:suscripcion.fechaInicio.timeIntervalSince1970+18000)
        datos["fechaFin"] = Date(timeIntervalSince1970:suscripcion.fechaFin.timeIntervalSince1970+18000)
        datos["farmacia"] = ["direccion":suscripcion.farmacia.direccion,"nombre":suscripcion.farmacia.nombre,"telefono":suscripcion.farmacia.telefono]
        var medicamentos = Array<Dictionary<String,Any>>()
        for medicamento in suscripcion.medicamentos
        {
            var medicamentoActual = Dictionary<String,Any>()
            medicamentoActual["nombre"] = medicamento.nombre
            medicamentoActual["nombreLab"] = medicamento.nombreLab
            medicamentoActual["dosisNumero"] = medicamento.dosisNumero
            medicamentoActual["dosisUnidad"] = medicamento.dosisUnidad
            medicamentoActual["unidadesPedidas"] = medicamento.unidadesPedidas
            medicamentoActual["dosisCaja"] = medicamento.dosisCaja
            medicamentos.append(medicamentoActual)
        }
        datos["medicamentos"] = medicamentos
        datos["cancelado"] = false
        datos["fechaAgendada"] = corregirFechas(fechas:suscripcion.fechaAgendada)
        if(suscripcion.fechaAgendada.count != 0){
            datos.updateValue(corregirFechas(fechas:suscripcion.fechaEntrega), forKey: "fechaEntrega")
        }
        else{
            datos.updateValue(nil, forKey: "fechaEntrega")
        }
        datos["ordenes"] = referenciarOrdenes(ordenes: suscripcion.ordenes)
        ref = db.collection("/clientes/\(Autenticar.getUser().uid)/suscripciones").addDocument(data: datos) { err in
            if let err = err {
                print("Error adding document: \(err)")
                completion(false, err)
            } else {
                print("Document added with ID: \(ref!.documentID)")
                self.agregarSuscripcionAOrden(ordenes: datos["ordenes"] as! Array<DocumentReference>, suscripcionId: ref!.documentID){ (correcto, error) in
                    if(correcto){
                        completion(true, nil)
                    }
                    else
                    {
                        completion(false, error)
                    }
                }
            }
        }
    }
    static func corregirFechas(fechas: Array<Date>)-> Array<Date>{
        var lista = Array<Date>()
        for fecha in fechas {
            let fechaActual = Date(timeIntervalSince1970:fecha.timeIntervalSince1970+18000)
            lista.append(fechaActual)
        }
        return lista
    }
    static func referenciarOrdenes(ordenes: Array<OrdenMedica>) -> Array<DocumentReference>{
        var lista = Array<DocumentReference>()
        for orden in ordenes{
            let userRef: DocumentReference = db.document("/clientes/\(Autenticar.getUser().uid)/ordenesMedicas/"+orden.id);
            lista.append(userRef)
        }
        return lista
    }
    static func agregarSuscripcionAOrden(ordenes: Array<DocumentReference>, suscripcionId: String, completion: @escaping (Bool, Error?) -> Void){
        let userRef: DocumentReference = db.document("/clientes/\(Autenticar.getUser().uid)/suscripciones/"+suscripcionId);
        var todoBien = true
        var contador = ordenes.count
        var errorGlob: Error? = nil
        for orden in ordenes{
            OrdenPersistence.anadirPedidoAOrden(tipo: "suscripciones", ordenMedica: orden, ref: userRef){
                (bien,error) in
                if(!bien)
                {
                    todoBien = false
                    errorGlob = error
                }
            }
            contador -= 1
            if(contador == 0)
            {
                if(todoBien)
                {
                  completion(true, nil)
                }
                else {
                    completion(false, errorGlob)
                }
            }
        }
    }
    static func deleteSuscripcion(suscripcionId: String, completion: @escaping (Bool, Error?) -> Void){
        let turno: DocumentReference = db.document("/clientes/\(Autenticar.getUser().uid)/suscripciones/"+suscripcionId);
        
        turno.updateData([
            "cancelado": true,
        ]) { err in
            if let err = err {
                print("Error updating document: \(err)")
                completion(false, err)
            } else {
                print("Document successfully updated/cancelled")
                completion(true, nil)
            }
        }
        
    }
    static func crearFechasAgendadas(fechaInicio: Date, fechaFin: Date, dia: Int, intervalo: Int, hora:Int, minutos: Int) -> Array<Date>{
        var lista = Array<Date>()
        var fechaInicioOficial = Calendar.current.date(bySettingHour: abs(hora-5), minute: minutos, second: 0, of: fechaInicio)!
        var calanderDate = Calendar.current.dateComponents([.day, .year, .month, .weekday], from: fechaInicioOficial)
        while calanderDate.weekday! != dia {
            fechaInicioOficial = Calendar.current.date(byAdding: .day, value: 1, to: fechaInicioOficial)!
            calanderDate = Calendar.current.dateComponents([.day, .year, .month, .weekday], from: fechaInicioOficial)
        }
        while fechaInicioOficial < fechaFin {
            lista.append(fechaInicioOficial)
            fechaInicioOficial = Calendar.current.date(byAdding: .day, value: 7*intervalo, to: fechaInicioOficial)!
        }
        print("Las fechas agendadas son:")
        print(lista)
        return lista
    }
    static func updateSuscripcion(suscripcion: Suscripcion, fechasAgendadasNuevas: Array<Date>, completion: @escaping (Bool, Error?) -> Void){
        let documento: DocumentReference = db.document("/clientes/\(Autenticar.getUser().uid)/suscripciones/"+suscripcion.id);
        var listaPreFinal = Array(suscripcion.fechaAgendada[..<suscripcion.fechaEntrega.count])
        listaPreFinal.append(contentsOf: fechasAgendadasNuevas)
        let listaFinal = corregirFechas(fechas: listaPreFinal)
        documento.updateData([
            "fechaAgendada": listaFinal
        ]) { err in
            if let err = err {
                print("Error updating document: \(err)")
                completion(false, err)
            } else {
                print("Document successfully updated")
                completion(true, nil)
            }
        }
    }
}
