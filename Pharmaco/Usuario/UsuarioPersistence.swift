//
//  UsuarioPersistence.swift
//  Pharmaco
//
//  Created by Nicolas Caceres on 14/11/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation
import Firebase

class UsuarioPersistence
{
    static private let db = Firestore.firestore()
   
    static func postUsuario (usuario: Usuario,completion: @escaping (Bool, Error?) -> Void) {
        var datos = [String:Any?]()
        datos["email"] = usuario.email
        datos["eps"] = usuario.eps
        datos["fechaNacimiento"] = Date(timeIntervalSince1970:usuario.fechaNacimiento.timeIntervalSince1970+18000)
        datos["genero"] = usuario.genero
        datos["nombre"] = usuario.nombre
        datos["numDocumento"] = usuario.numDocumento
        datos["tipoDocumento"] = usuario.tipoDocumento
        datos["fechaCreacionCuenta"] = Date()
        datos["plataforma"] = "IOS"
        datos["sesionActiva"] = false
        let date = Date()
        let calendar = Calendar.current
        let edad = calendar.component(.year, from: date) - calendar.component(.year, from: Date(timeIntervalSince1970:usuario.fechaNacimiento.timeIntervalSince1970+18000))
        datos["edad"] = edad
        //print(datos)
        db.collection("/clientes").document(usuario.id).setData(datos) { err in
            if let err = err {
                print("Error adding or updating document: \(err)")
                completion(false, err)
            } else {
                print("Document added or updated with ID:",usuario.id)
                completion(true, nil)

                
            }
        }
    }
    static func getUsuario (id: String, completion: @escaping (Usuario?, Error?) -> Void) {
        let docRef = db.collection("/clientes").document(id)

        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let id = document.documentID
                let email = document.get("email") as! String
                let eps = document.get("eps") as! String
                let fechaNacimientoTemp = document.get("fechaNacimiento") as? Timestamp
                let fechaNacimiento = Date(timeIntervalSince1970: TimeInterval(fechaNacimientoTemp!.seconds)-18000)
                let genero = document.get("genero") as! String
                let nombre = document.get("nombre") as! String
                let numDocumento = document.get("numDocumento") as! String
                let tipoDocumento = document.get("tipoDocumento") as! String
                let usuario = Usuario(id: id, email: email, eps: eps, fechaNacimiento: fechaNacimiento, genero: genero, nombre: nombre, numDocumento: numDocumento, tipoDocumento: tipoDocumento)
                //print("Document data:",usuario.id,usuario.email,usuario.eps,usuario.fechaNacimiento,usuario.genero,usuario.nombre,usuario.numDocumento,usuario.tipoDocumento)
                completion(usuario, nil)
            } else {
                print("Document does not exist")
                completion(nil, error)
            }
        }
    }
    static func estaLoggeado (email: String, completion: @escaping (Bool, Error?) -> Void) {
        let docRef = db.collection("/clientes").whereField("email", isEqualTo: email)

        docRef.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                completion(false, err)
                
            } else {
                for document in querySnapshot!.documents {
                    
                    let sesionActiva = document.get("sesionActiva") as! Bool
                    if(sesionActiva)
                    {
                        completion(true, nil)
                    }
                    else{
                        completion(false, nil)
                    }
                    
            }
            }
        }
        completion(false,nil)
    }
    static func loggearUsuario (email:String ) {
        let docRef = db.collection("/clientes").whereField("email", isEqualTo: email)

        docRef.getDocuments()
         { (querySnapshot, err) in
             if let err = err {
                 print("Error getting documents: \(err)")
             } else {
                for document in querySnapshot!.documents {
                    let userRef = db.collection("/clientes").document(document.documentID)
                        userRef.updateData([
                            "plataforma": "IOS",
                            "sesionActiva":true
                        ])
                }
             }
         }
    }
    static func desloggearUsuario (email:String ) {
        let docRef = db.collection("/clientes").whereField("email", isEqualTo: email)

        docRef.getDocuments()
         { (querySnapshot, err) in
             if let err = err {
                 print("Error getting documents: \(err)")
             } else {
                for document in querySnapshot!.documents {
                    let userRef = db.collection("/clientes").document(document.documentID)
                        userRef.updateData([
                            "sesionActiva":false
                        ])
                }
             }
         }
    }
}
