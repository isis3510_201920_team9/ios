//
//  Usuario.swift
//  Pharmaco
//
//  Created by Nicolas Caceres on 16/11/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation

class Usuario {
    var id: String
    var email: String
    var eps: String
    var fechaNacimiento: Date
    var genero: String
    var nombre: String
    var numDocumento: String
    var tipoDocumento: String

    
    init(id: String, email: String, eps: String, fechaNacimiento: Date, genero: String, nombre: String, numDocumento: String, tipoDocumento: String) {
        self.id = id
        self.email = email
        self.eps = eps
        self.fechaNacimiento = fechaNacimiento
        self.genero = genero
        self.nombre = nombre
        self.numDocumento = numDocumento
        self.tipoDocumento = tipoDocumento
    }
}
