//
//  Pharmaco.swift
//  Pharmaco
//
//  Created by Nicolas Caceres on 10/2/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation
import Firebase

class Pharmaco {
    private let db = Firestore.firestore()
    
    func getUsuarios () {
        db.collection("users").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                }
            }
        }
        
    }
    
    func postUsuario (usuario: [String: Any]) {
        var ref: DocumentReference? = nil
        ref = db.collection("users").addDocument(data: usuario) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
            }
        }
    }
}

