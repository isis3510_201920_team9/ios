//
//  NotificacionPersistence.swift
//  Pharmaco
//
//  Created by Nicolas Caceres on 12/12/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation
import Firebase

class NotificacionPersistence
{
    static private let db = Firestore.firestore()
    
    static func getNotificaciones (completion: @escaping ([Notificacion], Error?) -> Void) {
        var lista = [Notificacion]()
        db.collection("/clientes/\(Autenticar.getUser().uid)/notificaciones").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                completion(lista, err)
                
            } else {
                for document in querySnapshot!.documents {
                    //print("\(document.documentID) => \(document.data())")
                    let  mensaje = document.get("mensaje") as! String
                    let  mensajeCorto = document.get("mensajeCorto") as! String
                    let  tipo = document.get("tipo") as! String
                    let  titulo = document.get("titulo") as! String
                    let fechaSeg = document.get("fecha") as! Timestamp
                    let fecha = Date(timeIntervalSince1970: TimeInterval(fechaSeg.seconds)-18000)
                    if(tipo == "turno")
                    {
                        let photo = UIImage(named: "clock.fill")
                        let actual = Notificacion(titulo: titulo, mensaje:mensaje, fecha:fecha, mensajeCorto:mensajeCorto, tipo: tipo, image: photo)
                        lista.append(actual)
                    }
                    else if (tipo == "suscripcion")
                    {
                        let photo = UIImage(named: "cart.fill")
                        let actual = Notificacion(titulo: titulo, mensaje:mensaje, fecha:fecha, mensajeCorto:mensajeCorto, tipo: tipo, image: photo)
                        lista.append(actual)
                    }
                    else{
                        let photo = UIImage(named: "car.fill")
                        let actual = Notificacion(titulo: titulo, mensaje:mensaje, fecha:fecha, mensajeCorto:mensajeCorto, tipo: tipo, image: photo)
                        lista.append(actual)
                    }
                    if(lista.count == querySnapshot!.documents.count)
                    {
                        completion(lista, nil)
                    }
                }
            }
        }
    }
}


