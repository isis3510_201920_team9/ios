
//
//  NotificationTableViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/2/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class NotificationTableViewController: UITableViewController {

    var notifications = [Notificacion]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadNotificationSamples()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadNotificationSamples()
    }
    @IBAction func refresh(_ sender: UIRefreshControl) {
        NotificacionPersistence.getNotificaciones(){ (notificaciones, error) in
            self.notifications = notificaciones
            self.tableView.reloadData()
            sender.endRefreshing()
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "NotificationViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? NotificationTableViewCell else {
            fatalError("No se pudo instanciar como la tabla requerida")
        }
        
        let notification = notifications[indexPath.row]
        // Configure the cell...
        
        if notifications.isEmpty {
            
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy hh:MM:ss"
        
        cell.nameLabel.text = notification.titulo
        cell.descriptionLabel.text = notification.mensaje
        cell.imageView?.image = notification.image
        cell.fecha.text = formatter.string(from: notification.fecha)

        return cell
    }

    func loadNotificationSamples() {
        NotificacionPersistence.getNotificaciones(){ (notificaciones, error) in
            self.notifications = notificaciones
            self.tableView.reloadData()
        }
    }
    
}
