//
//  Notification.swift
//  Pharmaco
//
//  Created by Admin on 10/2/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class Notificacion{
    var titulo: String
    var mensaje: String
    var fecha: Date
    var mensajeCorto: String
    var tipo: String
    var image: UIImage?
    
    init(titulo: String, mensaje:String, fecha:Date, mensajeCorto:String, tipo: String, image: UIImage?){
        self.titulo = titulo
        self.mensaje = mensaje
        self.fecha = fecha
        self.mensajeCorto = mensajeCorto
        self.tipo = tipo
        self.image = image
    }
}

