//
//  OrderUpdateViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/9/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class OrderUpdateViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var medsContainer: UIView!
    @IBAction func cancelarButton(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    
    @IBAction func guardarButton(_ sender: UIButton) {
        
        if nombreTextField.text?.count == 0 {
            medsContainer.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            medsContainer.layer.borderWidth = 1
            medsContainer.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo actualizar", message: "Por favor agregue uno o más medicamentos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if especialidadTextField.text?.count == 0 {
            especialidadTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            especialidadTextField.layer.borderWidth = 1
            especialidadTextField.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo actualizar", message: "Por favor agregue uno o más medicamentos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if order?.frecuencias.count == 0 {
            nombreTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            nombreTextField.layer.borderWidth = 1
            nombreTextField.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo actualizar", message: "Por favor llene todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        order!.doctor = nombreTextField.text!
        order!.especialidad = especialidadTextField.text!
        OrdenPersistence.updateOrdenMedica(ordenMedica: order!) { (exitoso:Bool, error) in
            if exitoso {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "OrderListUpdate"), object: nil)
            } else {
                print("\(error)")
            }
        }
        
        if !LoginViewController.conn! {
            let alert = UIAlertController(title: "No hay conexión", message: "En el momento en que se recupere conexión, se registrará el cambio", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                (alert: UIAlertAction) -> Void in
                self.performSegueToReturnBack()
            }))
            
            self.present(alert, animated: true)
        } else {performSegueToReturnBack()}
    }
    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var especialidadTextField: UITextField!
    
    var order: OrdenMedica?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        nombreTextField.delegate = self
        especialidadTextField.delegate = self
        nombreTextField.clearsOnBeginEditing = false
        nombreTextField.insertText(order!.doctor)
        especialidadTextField.insertText(order!.especialidad)
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    
    var label: UILabel?
    
    @objc func showToastNoConn() {
        self.UI {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    @objc func showToastConn() {
        self.UI {
            if self.label != nil {
                self.hideToast(toast: self.label!)
            }
            
            self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
            self.label = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !LoginViewController.conn! && self.label==nil  {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    let ACCEPTABLE_CHARACTERS = "AÁBCDEÉFGHIÍJKLMNOÓPQRSTUÚVWXYZaábcdeéfghiíjklmnoópqrstuúvwxyz. "
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OrderMedsUpdateSegue" {
            let vc = segue.destination as? OrderAddMedsUpdateViewController
            vc!.order = order
        }
    }
}
