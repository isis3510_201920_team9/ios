//
//  OrderAddMedsUpdateViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/10/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class OrderAddMedsUpdateViewController: UIViewController {

    @IBAction func addButton(_ sender: UIButton) {
        performSegue(withIdentifier: "addMedOrderUpdateSegue", sender: nil)
    }
    
    var order: OrdenMedica?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tableMedsUpdateSegue" {
            let vc = segue.destination as? OrderMedsUpdateTableViewController
            vc?.order = order
        }
        else if segue.identifier == "addMedOrderUpdateSegue" {
            let vc = segue.destination as? OrderAddMedUpdateViewController
            vc?.order = order
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
