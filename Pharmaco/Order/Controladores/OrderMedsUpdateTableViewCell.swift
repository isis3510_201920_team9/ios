//
//  OrderMedsUpdateTableViewCell.swift
//  Pharmaco
//
//  Created by Admin on 10/10/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class OrderMedsUpdateTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var laboratorioLabel: UILabel!
    @IBOutlet weak var cantidadLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
