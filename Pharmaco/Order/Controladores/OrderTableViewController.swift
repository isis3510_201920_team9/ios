//
//  OrderTableViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/2/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class OrderTableViewController: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    @IBAction func refresh(_ sender: UIRefreshControl) {
        OrdenPersistence.getOrdenesVigentes { (list, err) in
            self.orders = list
            self.tableView.reloadData()
            sender.endRefreshing()
        }
    
    }
    
    var orders = [OrdenMedica]()
    weak var order: OrdenMedica?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadList), name: NSNotification.Name(rawValue: "OrderListUpdate"), object: nil)
        
        OrdenPersistence.getOrdenesVigentes(){(ordenes, error) in
            self.orders = ordenes
            self.tableView.reloadData()
        }
    }
    
    @objc func reloadList()
    {
        OrdenPersistence.getOrdenesVigentes {(lista, error) in
            self.orders = lista
            self.tableView.reloadData()
        }
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "¡Hola!"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }

    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Parece que no tienes ninguna orden agregada"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let index = tableView.indexPathForSelectedRow{
            
            order = orders[index.row]
            if segue.identifier == "solicitarTurnoSegue"{
                let vc = segue.destination as? DomicilioViewController
                vc?.order = order
            }
            else if (segue.identifier == "ordenTurnoSegue") {
                let vc = segue.destination as? OrdenTurnoViewController
                vc?.order = order
            }
            else if segue.identifier == "crearSuscripcionOrdenSegue" {
                let vc = segue.destination as? OrdenSuscripcionViewController
                vc?.orden = order
            }
        }
        if segue.identifier == "editarOrdenSegue" {
            let vc = segue.destination as? OrderUpdateViewController
            vc?.order = order
        }
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentification = "OrderTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentification, for: indexPath) as? OrderTableViewCell else {
            fatalError("No se pudo instanciar a OrderTableViewCell")
        }
        
        let order = orders[indexPath.row]
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy hh:MM:ss"
        
        // Configure the cell...
        cell.nameLabel.text = "Orden del médico general"
        cell.fechaLabel.text = formatter.string(from: order.fecha)
        cell.descriptionLabel.text = "Número de medicamentos \(order.frecuencias.count)"
        cell.photoImageView.image = UIImage(named: "doc.fill")
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let optionMenu = UIAlertController(title: nil, message: "Selecciona una opción", preferredStyle: .actionSheet)
        
        let pedirTurnoAction = UIAlertAction(title: "Solicitar turno en farmacia", style: .default, handler: {
            (alert: UIAlertAction) -> Void in
            self.performSegue(withIdentifier: "ordenTurnoSegue", sender: nil)
        })
        
        let pedirDomicilioAction = UIAlertAction(title: "Solicitar domicilio", style: .default, handler: {
            (alert: UIAlertAction) -> Void in
            self.performSegue(withIdentifier: "solicitarTurnoSegue", sender: nil)
        })
        
        let suscribirseEnFarmaciaAction = UIAlertAction(title: "Suscribirse en una farmacia", style: .default, handler: {
            (alert: UIAlertAction) -> Void in
            self.performSegue(withIdentifier: "crearSuscripcionOrdenSegue", sender: nil)
        })
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel)
        
        optionMenu.addAction(pedirTurnoAction)
        optionMenu.addAction(pedirDomicilioAction)
        optionMenu.addAction(suscribirseEnFarmaciaAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView,
                            trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        // Write action code for the trash
        let TrashAction = UIContextualAction(style: .normal, title:  "Eliminar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            let alertMenu = UIAlertController(title: "Eliminar", message: "¿Eliminar orden?", preferredStyle: .alert)
            
            let confirmar = UIAlertAction(title: "Confirmar", style: .destructive, handler: {
                (alert: UIAlertAction) -> Void in
                //TODO
                OrdenPersistence.deleteOrden(ordenId: self.orders[indexPath.row].id) { (exitoso: Bool, error) in
                    if exitoso {
                        self.orders.remove(at: indexPath.row)
                        self.tableView.reloadData()
//                        OrdenPersistence.getOrdenesVigentes(){(lista,error) in
//                            self.orders = lista
//
//                        }
                    } else {
                        fatalError("\(String(describing: error))")
                    }
                }
            })
            let cancelar = UIAlertAction(title: "Cancelar", style: .default)
            
            alertMenu.addAction(confirmar)
            alertMenu.addAction(cancelar)
            self.present(alertMenu, animated: true, completion: nil)
            success(true)
        })
        TrashAction.backgroundColor = .red
        
        let MoreAction = UIContextualAction(style: .normal, title:  "Editar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            self.order = self.orders[indexPath.row]
            for vc in self.children {
                if let child = vc as? OrderUpdateViewController{
                    child.order = self.order
                }
            }
            self.performSegue(withIdentifier: "editarOrdenSegue", sender: nil)
            success(true)
        })
        MoreAction.backgroundColor = .gray
        
        
        return UISwipeActionsConfiguration(actions: [TrashAction,MoreAction])
    }
}
