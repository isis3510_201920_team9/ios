//
//  OrderFormViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/2/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit
import FirebaseMLVision

class OrderFormViewController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var textRecognizer: VisionTextRecognizer!
    
    @IBOutlet weak var medsContainer: UIView!
    
    var order: OrdenMedica?
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vision = Vision.vision()
        textRecognizer = vision.onDeviceTextRecognizer()
        nombreEspecialistaText.delegate = self
        especialidad.delegate = self
        nombreEspecialistaText.delegate = self
        especialidad.delegate = self
        
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    
    var label: UILabel?
    
    @objc func showToastNoConn() {
        self.UI {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    @objc func showToastConn() {
        self.UI {
            if self.label != nil {
                self.hideToast(toast: self.label!)
            }
            
            self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
            self.label = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !LoginViewController.conn! && self.label==nil  {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 40
    }
    
    //MARK: Text recognition functions
    
    func runTextRecognition(with image:UIImage) {
        let visionImage = VisionImage(image: image)
        textRecognizer.process(visionImage, completion: {(features, error) in
            self.processResult(from: features, error: error)
        })
    }
    
    func processResult(from text: VisionText?, error: Error?){
        guard let features = text else { return }
        var i = 0
        var meds = false
        var frecuencias = [Frecuencia]()
        var medicamentos = [Medicamento]()
        for block in features.blocks{
            if block.text.contains("Medicamento"){
                meds = true
            }
            if meds {
                if block.text.contains("FECHA DE") || block.text.contains("Apreciado") || block.text.contains("Los medicamentos") || block.text.contains("*Los medicamentos") || block.text.contains("medicamentos únicamente") || block.text.contains("definido") {
                    break;
                }
//                print("Numero: \(i) \(block.text)")
                if(!block.text.contains("No") && !block.text.contains("Medicamento y Prescripción") && !block.text.contains("CruzVerde") && !block.text.contains("cruzVerde") && !block.text.contains("Cruz") && !block.text.contains("cruz") && !block.text.contains("Cantidad total") && !block.text.contains("Cantidad") && !block.text.contains("total") && !block.text.contains("Cantidad")){
                    var cont = 0
                    
                    var medicamento: Medicamento?
                    var frecuencia: Frecuencia?
                    
                    for linea in block.lines{
                        cont+=1
                        
                        var nombre = ""
                        var dosisNumero = 0
                        var dosisUnidad = ""
                        var unidadesPedidas = 1
                        var dosisCaja = 0
                        
                        var unidades = 0
                        var numero = 0
                        var unidadFrecuencia = ""
                        var periodo = 0
                        var unidadPeriodo = ""
                        if linea.elements[0].text != "Tomar"{
                            nombre = linea.elements[0].text
                            dosisNumero = Int(linea.elements[linea.elements.count-1].text.trimmingCharacters(in: CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyz"))) ?? 1
                            dosisCaja = Int(linea.elements[linea.elements.count-1].text.trimmingCharacters(in: CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyz"))) ?? 1
                            if linea.elements[linea.elements.count-1].text.contains("mg") {
                                dosisUnidad = "mg"
                            }
                            
                        }
                        else
                        {
                            if linea.elements.count < 11 {
                                let alert = UIAlertController(title: "Ocurrió un error", message: "Por favor revisa los medicamentos agregados y vuelve a tomar la foto", preferredStyle: .alert)

                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

                                self.present(alert, animated: true)
                            } else {
                                unidades = Int(linea.elements[3].text) ?? 1
                                numero = Int(linea.elements[9].text) ?? 1
                                unidadFrecuencia = linea.elements[7].text
                                periodo = Int(linea.elements[6].text) ?? 1
                                unidadPeriodo = linea.elements[10].text
                            }
                            
                        }
                                            
                        if cont == 1 {
                            medicamento = Medicamento(nombre: nombre, nombreLab: "MK", dosisNumero: dosisNumero, dosisUnidad: dosisUnidad, unidadesPedidas: unidadesPedidas, dosisCaja: dosisCaja)
                        } else {
                            frecuencia = Frecuencia(unidades: unidades, numero: numero, unidadFrecuencia: unidadFrecuencia, periodo: periodo, unidadPeriodo: unidadPeriodo, medicamento: medicamento!)
                            
                            medicamentos += [medicamento!]
                            frecuencias += [frecuencia!]
                            
                            medicamento = nil
                            frecuencia = nil
                            
                            cont = 0
                        }
                        i+=1
                    }
                }
            }
        }
        let vc = self.children.first as! addMedViewController
        order = OrdenMedica(especialidad: "Prueba", borrada: false, doctor: "Prueba", fecha: Date(), tipo: "Foto", audio: "", foto: "", id: "", suscripciones: [Suscripcion](), domicilios: [Domicilio](), turnos: [Turno](), frecuencias: frecuencias)
        if order?.frecuencias.count == 0 {
            let alert = UIAlertController(title: "No se encontró una orden", message: "Por favor vuelve a tomar la foto", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
        }
        vc.order = order
        vc.updateMeds()
    
    }

    func createOrder(){
        let date = Date()
        
        order = OrdenMedica(especialidad: "temporal", doctor: "temporal", fecha: date, tipo: "temporal", audio: "", foto: "", id: "")
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        self.BG {
            self.runTextRecognition(with: image)
        }
    }
    
    @IBAction func fotoButton(_ sender: UIButton) {
        let optionMenu = UIAlertController(title: nil, message: "Selecciona una opción", preferredStyle: .actionSheet)
        
        let photoAction = UIAlertAction(title: "Tomar una foto", style: .default, handler: {
            (alert: UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let landscapeAlert = UIAlertController(title: "Aviso", message: "Por favor asegúrate de tomar la foto en modo horizontal", preferredStyle: .alert)
                
                let action = UIAlertAction(title: "Ok", style: .default) { (action: UIAlertAction) in
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.sourceType = .camera;
                    imagePicker.allowsEditing = true
                    self.present(imagePicker, animated: true)
                }
                
                landscapeAlert.addAction(action)
                self.present(landscapeAlert, animated: true)
            }
        } )
        
        let galleryAction = UIAlertAction(title: "Seleccionar foto", style: .default, handler: {
            (action: UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel)
        
        optionMenu.addAction(photoAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func cancelOrderForm(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    
    let ACCEPTABLE_CHARACTERS = "AÁBCDEÉFGHIÍJKLMNOÓPQRSTUÚVWXYZaábcdeéfghiíjklmnoópqrstuúvwxyz "
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
//        let filtered = string.components(separatedBy: cs).joined(separator: "")
//
//        return (string == filtered)
//    }
    
    @IBOutlet weak var nombreEspecialistaText: UITextField!
    @IBOutlet weak var especialidad: UITextField!
    
    @IBAction func saveOrder(_ sender: UIButton) {
        
        guard let _ = nombreEspecialistaText.text, nombreEspecialistaText.text?.count != 0 else {
            nombreEspecialistaText.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            nombreEspecialistaText.layer.borderWidth = 1
            nombreEspecialistaText.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo actualizar", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        guard let _ = especialidad.text, nombreEspecialistaText.text?.count != 0 else {
            especialidad.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            especialidad.layer.borderWidth = 1
            especialidad.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo actualizar", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if order?.frecuencias.count == 0 {
            
            medsContainer.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            medsContainer.layer.borderWidth = 1
            medsContainer.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo actualizar", message: "Por favor agregue uno o más medicamentos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        order?.doctor = nombreEspecialistaText.text!
        order?.especialidad = especialidad.text!
        order?.tipo = "texto"
        
        OrdenPersistence.postOrdenMedica(ordenMedica: order!){(exitoso, error) in
            if !exitoso {
                fatalError("\(String(describing: error))")
            } else {                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "OrderListUpdate"), object: nil)
            }
        }
        if !LoginViewController.conn! {
            let alert = UIAlertController(title: "No hay conexión", message: "En el momento en que se recupere conexión, se registrará el cambio", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                (alert: UIAlertAction) -> Void in
                self.performSegueToReturnBack()
            }))
            
            self.present(alert, animated: true)
        } else {performSegueToReturnBack()}
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "myEmbeddedSegue") {
            createOrder()
            let childViewController = segue.destination as! addMedViewController
            childViewController.order = order
        }
    }
}

extension UIViewController {
    func performSegueToReturnBack()  {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

