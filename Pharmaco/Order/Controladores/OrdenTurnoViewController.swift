//
//  OrdenTurnoViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/6/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit
import MapKit

class OrdenTurnoViewController: UIViewController, CLLocationManagerDelegate {
    
    var order: OrdenMedica?
    var farmacias = [Farmacia]()
    var farmaciaEscogida: Farmacia?
    var mark: FarmaciaMarker?
    @IBOutlet weak var medsContainer: UIView!
    
    @IBAction func crearButton(_ sender: UIButton) {
        let childV = self.children.first as? OrdenTurnoTableViewController
        
        let medsAPedir = childV?.medsAPedir
        
        if farmaciaEscogida == nil {
            map.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            map.layer.borderWidth = 1
            map.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor seleccione una farmacia", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if datePicker.date < Date() {
            datePicker.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            datePicker.layer.borderWidth = 1
            datePicker.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor seleccione una fecha válida", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if medsAPedir?.count == 0 {
            medsContainer.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            medsContainer.layer.borderWidth = 1
            medsContainer.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor selecciona uno o más medicamentos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        let turno = Turno(especial: false, observacionesEspecial: nil, fechaAgendada: datePicker.date, QR: "", farmacia: farmaciaEscogida!, medicamentos: medsAPedir!, ordenes: [order!])
        
        TurnoPersistence.postTurno(turno: turno) { (exitoso: Bool, error) in
            if !exitoso{
                fatalError("\(error)")
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TurnoListUpdate"), object: nil)
            }
        }
        
        order!.turnos += [turno]
        if !LoginViewController.conn! {
            let alert = UIAlertController(title: "No hay conexión", message: "En el momento en que se recupere conexión, se registrará el cambio", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                (alert: UIAlertAction) -> Void in
                self.performSegueToReturnBack()
            }))
            
            self.present(alert, animated: true)
        } else {performSegueToReturnBack()}
    }
    
    @IBAction func cancelButton(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var datePicker: UIDatePicker!
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        map.delegate = self
        setupUI()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        loadFarmacias()
        
        datePicker.locale = Locale(identifier: "es_ES")
        
        if let initialLocation = locationManager.location{
            centerMapOnLocation(location: initialLocation)
        }
        map.showsUserLocation = true
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    
    var label: UILabel?
    
    @objc func showToastNoConn() {
        self.UI {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    @objc func showToastConn() {
        self.UI {
            if self.label != nil {
                self.hideToast(toast: self.label!)
            }
            
            self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
            self.label = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !LoginViewController.conn! && self.label==nil  {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    func setupUI(){
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        map.setRegion(coordinateRegion, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "Capital"
        
        if annotation is FarmaciaMarker {
            if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) {
                annotationView.annotation = annotation
                return annotationView
            } else {
                let annotationView = MKPinAnnotationView(annotation:annotation, reuseIdentifier:identifier)
                annotationView.isEnabled = true
                annotationView.canShowCallout = true
                
                let btn = UIButton(type: .detailDisclosure)
                btn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
                annotationView.rightCalloutAccessoryView = btn
                return annotationView
            }
        }
        
        return nil
    }
    
    @objc func buttonAction(sender: UIButton!){
        
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let capital = view.annotation as! FarmaciaMarker
        let placeName = capital.title
        let placeInfo = "capital.info"
        let ac = UIAlertController(title: placeName, message: placeInfo, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
    
    func loadFarmacias () {
        FarmaciaPersistence.getFarmacias(){(lista, error) in
            self.farmacias = lista
            for farmacia in self.farmacias {
                self.map.addAnnotation(FarmaciaMarker(farmacia: farmacia))
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let eleccion = view.annotation as? FarmaciaMarker
        farmaciaEscogida = eleccion?.farmacia
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OrdenTurnoMedsSegue" {
            let vc = segue.destination as! OrdenTurnoTableViewController
            vc.order = order
        }
    }    
}

extension OrdenTurnoViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotation? {
        guard annotation is FarmaciaMarker else {
            return nil
        }
        let _identifier = "marker"
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: _identifier) as? MKMarkerAnnotationView
        {
            dequeuedView.annotation = annotation
            view = dequeuedView
        }
        else{
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: _identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view as? MKAnnotation
    }
}
