//
//  OrderUpdateMedViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/10/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class OrderUpdateMedViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    @IBOutlet weak var nombreMedTextField: UITextField!
    @IBOutlet weak var dosisPorUnidadTextField: UITextField!
    @IBOutlet weak var unidadDosisPicker: UIPickerView!
    @IBOutlet weak var cantidadUnidadesTextField: UITextField!
    @IBOutlet weak var tomarTextField: UITextField!
    @IBOutlet weak var cadaTextField: UITextField!
    @IBOutlet weak var perdidoPicker: UIPickerView!
    @IBOutlet weak var porTextField: UITextField!
    @IBOutlet weak var unidadPeriodoPicker: UIPickerView!
    
    var pickerData1: [String] = [String]()
    var pickerData2: [String] = [String]()
    var pickerData3: [String] = [String]()
    
    
    var frec: Frecuencia?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        dosisPorUnidadTextField.delegate = self
        cantidadUnidadesTextField.delegate = self
        tomarTextField.delegate = self
        cadaTextField.delegate = self
        porTextField.delegate = self
        
        nombreMedTextField.delegate = self
        
        nombreMedTextField.insertText(frec!.medicamento.nombre)
        dosisPorUnidadTextField.insertText(String(frec!.medicamento.dosisNumero))
        cantidadUnidadesTextField.insertText(String(frec!.medicamento.unidadesPedidas))
        tomarTextField.insertText(String(frec!.unidades))
        cadaTextField.insertText(String(frec!.numero))
        porTextField.insertText(String(frec!.periodo))

        self.hideKeyboardWhenTappedAround()
        
        self.unidadDosisPicker.delegate = self
        self.unidadDosisPicker.dataSource = self
        
        self.perdidoPicker.delegate = self
        self.perdidoPicker.dataSource = self
        
        self.unidadPeriodoPicker.delegate = self
        self.unidadPeriodoPicker.dataSource = self
        
        pickerData1 = ["mg", "cm", "ml", "oz"]
        pickerData2 = ["horas", "dias"]
        pickerData3 = ["días", "semanas", "meses"]
        
       NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
        }
        
        var label: UILabel?
        
        @objc func showToastNoConn() {
            self.UI {
                self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
            }
        }
        
        @objc func showToastConn() {
            self.UI {
                if self.label != nil {
                    self.hideToast(toast: self.label!)
                }
                
                self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
                self.label = nil
            }
        }
        
        override func viewDidAppear(_ animated: Bool) {
            if !LoginViewController.conn! && self.label==nil  {
                self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
            }
    }
    
    let ACCEPTABLE_CHARACTERS = "AÁBCDEÉFGHIÍJKLMNOÓPQRSTUÚVWXYZaábcdeéfghiíjklmnoópqrstuúvwxyz.1234567890 "
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        if textField.tag == 0 {
            return count <= 2 && (string == filtered)
        }
        else {
            return count <= 35 && (string == filtered)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return pickerData1.count
        }
        else if pickerView.tag == 2 {
            return pickerData2.count
        }
        else{
            return pickerData3.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return pickerData1[row]
        }
        else if pickerView.tag == 2 {
            return pickerData2[row]
        }
        else{
            return pickerData3[row]
        }
        
        
    }
    
    
    @IBAction func cancelarButton(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    
    @IBAction func guardarButton(_ sender: UIButton) {
        let medicamento = Medicamento(nombre: nombreMedTextField.text!,
                                      nombreLab: "mk",
                                      dosisNumero: Int(dosisPorUnidadTextField!.text!)!,
                                      dosisUnidad: pickerData1[unidadDosisPicker.selectedRow(inComponent: 0)],
                                      unidadesPedidas: Int(cantidadUnidadesTextField.text!)!,
                                      dosisCaja: Int(dosisPorUnidadTextField.text!)!)
        
        frec!.unidades = Int(tomarTextField.text!)!
        frec!.numero = Int(cadaTextField.text!)!
        frec!.unidadFrecuencia = pickerData2[perdidoPicker.selectedRow(inComponent: 0)]
        frec!.unidadPeriodo = pickerData3[unidadPeriodoPicker.selectedRow(inComponent: 0)]
        frec!.medicamento = medicamento
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "orderAddMedUpdateReload"), object: nil)
        
        performSegueToReturnBack()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
