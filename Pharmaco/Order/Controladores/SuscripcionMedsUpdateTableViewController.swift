//
//  SuscripcionMedsUpdateTableViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/11/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class SuscripcionMedsUpdateTableViewController: UITableViewController {
    
    var suscripcion: Suscripcion?
    var meds = [Medicamento]()
    var medsAEscoger = [Medicamento]()
    var orders: [OrdenMedica]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        OrdenPersistence.getOrdenesVigentes{(lista, error) in
            self.orders = lista
            for order in self.orders! {
                for frecuencia in order.frecuencias {
                    self.meds.append(frecuencia.medicamento)
                }
            }
            self.tableView.reloadData()
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meds.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "orderMedsUpdateIdentifier", for: indexPath) as? SuscripcionMedsUpdateTableViewCell else {
            fatalError("No se pude realizar el downcast")
        }
        
        let med = meds[indexPath.row]
        // Configure the cell...
        cell.nombreLabel.text = med.nombre
        cell.laboratorioLabel.text = med.nombreLab
        cell.cantidadLabel.text = "\(med.unidadesPedidas)"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let med = meds[indexPath.row]
        if !medsAEscoger.contains(med) {
            medsAEscoger.append(med)
        }
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let medA = meds[indexPath.row]
        medsAEscoger.removeAll { (med: Medicamento) -> Bool in
            med == medA
        }
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
