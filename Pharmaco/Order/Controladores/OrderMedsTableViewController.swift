//
//  OrderMedsTableViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/4/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class OrderMedsTableViewController: UITableViewController {
    
    var order: OrdenMedica?
//    var booleanos: [Bool]?
    var medsApedir = [Medicamento]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsMultipleSelection = true
//        for _ in order!.frecuencias {
//            booleanos!.append(false)
//        }
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order!.frecuencias.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "orderMedsIdentifier"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? OrderMedsTableViewCell else {
            fatalError("No se pudo instanciar a la clase order meds table view cell")
        }
        
        let orderMed = order?.frecuencias[indexPath.row].medicamento
        
        cell.medNameLabel.text = "\(orderMed!.nombre) \(orderMed!.dosisNumero) \(orderMed!.dosisUnidad)"
        cell.labLabel.text = orderMed?.nombreLab
        cell.cantidadLabel.text = String(orderMed!.unidadesPedidas)
        // Configure the cell...
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        booleanos![indexPath.row] = !booleanos![indexPath.row]
        let medAgregar = order!.frecuencias[indexPath.row].medicamento
        if !medsApedir.contains(medAgregar) {
            medsApedir.append(medAgregar)
        }
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let medAgregar = order!.frecuencias[indexPath.row].medicamento
        medsApedir.removeAll(where: { (med: Medicamento) -> Bool in
            med == medAgregar
        })
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
