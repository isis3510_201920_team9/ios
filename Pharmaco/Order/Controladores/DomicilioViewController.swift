//
//  DomicilioViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/4/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit
import MapKit

class DomicilioViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {
    
    weak var order: OrdenMedica?
    var frecuencias: [Frecuencia]?
    var farmacias = [Farmacia]()
    var farmaciaEscogida: Farmacia?
    
    let locationManager = CLLocationManager()
    @IBOutlet weak var medsContainer: UIView!
    
    @IBAction func cancelarButton(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    
    @IBAction func crearDomicilioButton(_ sender: UIButton) {
        let child = self.children.first as? OrderMedsTableViewController
        
        let medicamentos = child!.medsApedir
        
        if direccionTextField.text!.count == 0 {
            direccionTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            direccionTextField.layer.borderWidth = 1
            direccionTextField.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if datePicker.date < Date() {
            datePicker.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            datePicker.layer.borderWidth = 1
            datePicker.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor selecciona una fecha válida", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if medicamentos.count == 0 {
            medsContainer.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            medsContainer.layer.borderWidth = 1
            medsContainer.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "seleccione uno o más medicamentos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if farmaciaEscogida == nil {
            map.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            map.layer.borderWidth = 1
            map.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor selecciona una farmacia", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if observacionesLabel.text!.count == 0 {
            observacionesLabel.insertText("No hay observaciones")
        }
        
        let domicilio = Domicilio(direccion: direccionTextField.text!, observaciones: observacionesLabel.text!, fechaAgendada: datePicker.date, costo: 0, QR: "QR", farmacia: farmaciaEscogida!, medicamentos: medicamentos, ordenes: [order!])
        
        DomicilioPersistence.postDomicilio(domicilio: domicilio) { (exitoso: Bool, error) in
            if exitoso {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DomicilioListUpdate"), object: nil)
            }
        }
        
        order?.domicilios.append(domicilio)
        if !LoginViewController.conn! {
            let alert = UIAlertController(title: "No hay conexión", message: "En el momento en que se recupere conexión, se registrará el cambio", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                (alert: UIAlertAction) -> Void in
                self.performSegueToReturnBack()
            }))
            
            self.present(alert, animated: true)
        } else {performSegueToReturnBack()}
    }
    @IBOutlet weak var map: MKMapView!
    
    @IBOutlet weak var direccionTextField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var observacionesLabel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        direccionTextField.delegate = self
        observacionesLabel.delegate = self
        
        map.delegate = self
        setupUI()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        loadFarmacias()
        if let initialLocation = locationManager.location{
            centerMapOnLocation(location: initialLocation)
        }
        map.showsUserLocation = true
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    
    let ACCEPTABLE_CHARACTERS_LABEL = "AÁBCDEÉFGHIÍJKLMNOÓPQRSTUÚVWXYZaábcdeéfghiíjklmnoópqrstuúvwxyz()1234567890 "
    let ACCEPTABLE_CHARACTERS_DIRECCION = "AÁBCDEÉFGHIÍJKLMNOÓPQRSTUÚVWXYZaábcdeéfghiíjklmnoópqrstuúvwxyz()#-.1234567890 "
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
                   let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                       return false
               }
        
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        if textField.tag == 1 {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS_LABEL).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered) && (count <= 35)
        }
        else{
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS_DIRECCION).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered) && (count <= 35)
        }
    }
    
    var label: UILabel?
    
    @objc func showToastNoConn() {
        self.UI {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    @objc func showToastConn() {
        self.UI {
            if self.label != nil {
                self.hideToast(toast: self.label!)
            }
            
            self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
            self.label = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !LoginViewController.conn! && self.label==nil  {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    func setupUI(){
        
        let _annotation = FarmaciaMarker(farmacia: Farmacia(coordinate: CLLocationCoordinate2D(latitude: 1.2835921, longitude: 103.8448966), direccion: "Carrera 13", telefono: "h", nombre: "Cruz verde", horario: "h"))
        map.addAnnotation(_annotation)
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        map.setRegion(coordinateRegion, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "Capital"        
        if annotation is FarmaciaMarker {
            if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) {
                annotationView.annotation = annotation
                return annotationView
            } else {
                let annotationView = MKPinAnnotationView(annotation:annotation, reuseIdentifier:identifier)
                annotationView.isEnabled = true
                annotationView.canShowCallout = true
                
                let btn = UIButton(type: .detailDisclosure)
                btn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
                annotationView.rightCalloutAccessoryView = btn
                return annotationView
            }
        }
        
        return nil
    }
    
    @objc func buttonAction(sender: UIButton!){
        
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let capital = view.annotation as! FarmaciaMarker
        let placeName = capital.title
        let placeInfo = "capital.info"
        let ac = UIAlertController(title: placeName, message: placeInfo, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is OrderMedsTableViewController {
            let vc = segue.destination as? OrderMedsTableViewController
            vc?.order = order
        }
    }
    
    func loadFarmacias () {
        FarmaciaPersistence.getFarmacias(){(lista, error) in
            self.farmacias = lista
            for farmacia in self.farmacias {
                self.map.addAnnotation(FarmaciaMarker(farmacia: farmacia))
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let eleccion = view.annotation as? FarmaciaMarker
        farmaciaEscogida = eleccion?.farmacia
    }
}

extension DomicilioViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotation? {
        guard annotation is FarmaciaMarker else {
            return nil
        }
        let _identifier = "marker"
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: _identifier) as? MKMarkerAnnotationView
        {
            dequeuedView.annotation = annotation
            view = dequeuedView
        }
        else{
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: _identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view as? MKAnnotation
    }
}
