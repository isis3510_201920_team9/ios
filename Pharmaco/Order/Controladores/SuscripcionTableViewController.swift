//
//  SuscripcionTableViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/6/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class SuscripcionTableViewController: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    var suscripciones = [Suscripcion]()
    var suscripcionEscogida: Suscripcion?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(reloadList), name: NSNotification.Name(rawValue: "SuscripcionListUpdate"), object: nil)
        SuscripcionPersistence.getSuscripcionesVigentes(){(lista, error) in
            self.suscripciones = lista
            self.tableView.reloadData()
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @objc func reloadList() {
        SuscripcionPersistence.getSuscripcionesVigentes{(lista, error) in
            self.suscripciones = lista
            self.tableView.reloadData()
        }
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "¡Hola!"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }

    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Parece que no tienes ninguna orden agregada"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suscripciones.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "suscripcionCell", for: indexPath) as? SuscripcionTableViewCell else {
            fatalError("No se pudo hacer el downcast")
        }
        
        let suscripcion = suscripciones[indexPath.row]
        
        // Configure the cell...
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:MM"
        formatter.locale = Locale(identifier: "es-ES")
        cell.nombreLabel.text = suscripcion.farmacia.nombre
        cell.diaLabel.text = formatter.weekdaySymbols![Calendar.current.component(.weekday, from: suscripcion.fechaAgendada[0])-1]
        cell.horaLabel.text = formatter.string(from: suscripcion.fechaAgendada[0])
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let index = tableView.indexPathForSelectedRow {
            if segue.identifier == "SuscripcionDetailSegue" {
                let vc = segue.destination as? SuscripcionDetailViewController
                vc?.suscripcion = suscripciones[index.row]
            }
        }
        if segue.identifier == "suscripcionUpdateSegue" {
            let vc = segue.destination as? SuscripcionUpdateViewController
            vc!.suscripcion = suscripcionEscogida
        }
    }
    
    override func tableView(_ tableView: UITableView,
                            trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        // Write action code for the trash
        let TrashAction = UIContextualAction(style: .normal, title:  "Eliminar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            let alertMenu = UIAlertController(title: "Eliminar", message: "¿Eliminar orden?", preferredStyle: .alert)
            
            let confirmar = UIAlertAction(title: "Confirmar", style: .destructive, handler: {
                (alert: UIAlertAction) -> Void in
                SuscripcionPersistence.deleteSuscripcion(suscripcionId: self.suscripciones[indexPath.row].id) { (exitoso, error) in
                    if exitoso {
                        self.suscripciones.remove(at: indexPath.row)
                        self.tableView.reloadData()
                    } else {
                        fatalError("\(String(describing: error))")
                    }
                }
                
            })
            let cancelar = UIAlertAction(title: "Cancelar", style: .default)
            
            alertMenu.addAction(confirmar)
            alertMenu.addAction(cancelar)
            self.present(alertMenu, animated: true, completion: nil)
            success(true)
        })
        TrashAction.backgroundColor = .red
        
        let MoreAction = UIContextualAction(style: .normal, title:  "Editar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            self.suscripcionEscogida = self.suscripciones[indexPath.row]
            self.performSegue(withIdentifier: "suscripcionUpdateSegue", sender: nil)
            success(true)
        })
        MoreAction.backgroundColor = .gray
        
        
        return UISwipeActionsConfiguration(actions: [TrashAction,MoreAction])
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
