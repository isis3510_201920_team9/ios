//
//  OrderAddMedUpdateViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/10/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class OrderAddMedUpdateViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    var order: OrdenMedica?
    
    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var dosisUnidadTextField: UITextField!
    @IBOutlet weak var dosisUnidadPicker: UIPickerView!
    @IBOutlet weak var cantidadUnidadesTextField: UITextField!
    @IBOutlet weak var cantidadTextField: UITextField!
    @IBOutlet weak var cantidadTiempoTextField: UITextField!
    @IBOutlet weak var unidadTiempoPicker: UIPickerView!
    @IBOutlet weak var cantidadPeriodoTextField: UITextField!
    @IBOutlet weak var periodoPicker: UIPickerView!
    
    var pickerData1: [String] = [String]()
    var pickerData2: [String] = [String]()
    var pickerData3: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nombreTextField.delegate = self
        
        self.hideKeyboardWhenTappedAround()
        
        dosisUnidadTextField.delegate = self
        cantidadUnidadesTextField.delegate = self
        cantidadTextField.delegate = self
        cantidadTiempoTextField.delegate = self
        cantidadPeriodoTextField.delegate = self
        
        self.dosisUnidadPicker.delegate = self
        self.dosisUnidadPicker.dataSource = self
        
        self.unidadTiempoPicker.delegate = self
        self.unidadTiempoPicker.dataSource = self
        
        self.periodoPicker.delegate = self
        self.periodoPicker.dataSource = self
        
        pickerData1 = ["mg", "cm", "ml", "oz"]
        pickerData2 = ["horas", "dias"]
        pickerData3 = ["días", "semanas", "meses"]
        
        
        // Do any additional setup after loading the view.
        
    NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
        }
        
        var label: UILabel?
        
        @objc func showToastNoConn() {
            self.UI {
                self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
            }
        }
        
        @objc func showToastConn() {
            self.UI {
                if self.label != nil {
                    self.hideToast(toast: self.label!)
                }
                
                self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
                self.label = nil
            }
        }
        
        override func viewDidAppear(_ animated: Bool) {
            if !LoginViewController.conn! && self.label==nil  {
                self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
            }
    }
    
    let ACCEPTABLE_CHARACTERS = "AÁBCDEÉFGHIÍJKLMNOÓPQRSTUÚVWXYZaábcdeéfghiíjklmnoópqrstuúvwxyz.1234567890 "
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        if textField.tag == 0 {
            return count <= 2 && (string == filtered)
        }
        else {
            return count <= 35 && (string == filtered)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return pickerData1.count
        }
        else if pickerView.tag == 2 {
            return pickerData2.count
        }
        else{
            return pickerData3.count
        }
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return pickerData1[row]
        }
        else if pickerView.tag == 2 {
            return pickerData2[row]
        }
        else{
            return pickerData3[row]
        }
        
        
    }
    
    @IBAction func guardarButton(_ sender: UIButton) {
        let medicina = Medicamento(nombre: nombreTextField.text!, nombreLab: "mk", dosisNumero: Int(dosisUnidadTextField.text!)!, dosisUnidad: pickerData1[dosisUnidadPicker.selectedRow(inComponent: 0)], unidadesPedidas: Int(cantidadUnidadesTextField.text!)!, dosisCaja: Int(cantidadUnidadesTextField.text!)!)
        
        let frecuencia = Frecuencia(unidades: Int(cantidadTextField.text!)!, numero: Int(cantidadTiempoTextField.text!)!, unidadFrecuencia: pickerData2[unidadTiempoPicker.selectedRow(inComponent: 0)], periodo: Int(cantidadPeriodoTextField!.text!)!, unidadPeriodo: pickerData3 [periodoPicker.selectedRow(inComponent: 0)], medicamento: medicina)
        order?.frecuencias.append(frecuencia)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "orderAddMedUpdateReload"), object: nil)
        performSegueToReturnBack()
        
    }
    
    @IBAction func cancelarButton(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
