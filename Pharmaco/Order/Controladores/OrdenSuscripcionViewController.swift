//
//  OrdenSuscripcionViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/6/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit
import MapKit

class OrdenSuscripcionViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, CLLocationManagerDelegate, UITextFieldDelegate {
    
    @IBAction func crearButton(_ sender: UIButton) {
        
        if farmacia == nil {
            map.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            map.layer.borderWidth = 1
            map.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor seleccione una farmacia", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if fechaFinPicker.date < Date() {
            fechaFinPicker.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            fechaFinPicker.layer.borderWidth = 1
            fechaFinPicker.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor seleccione una fecha válida", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if cantidadPeriodoTextField.text!.count == 0 {
            cantidadPeriodoTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            cantidadPeriodoTextField.layer.borderWidth = 1
            cantidadPeriodoTextField.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor complete todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        
        let date = timePicker.date
        let calendar = Calendar.current

        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        
        var fechas: Array<Date>?
        
        var medsAPedir = [Medicamento]()
        
        fechas = SuscripcionPersistence.crearFechasAgendadas(fechaInicio: Date(), fechaFin: self.fechaFinPicker.date, dia: DateFormatter().weekdaySymbols.firstIndex(of: DateFormatter().weekdaySymbols[self.semanaPicker.selectedRow(inComponent: 0)])!+1, intervalo: Int(self.cantidadPeriodoTextField.text!)!, hora: hour, minutos: minutes)
        
        if fechas!.count < 2 {
            self.fechaFinPicker.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            self.fechaFinPicker.layer.borderWidth = 1
            self.fechaFinPicker.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor asegurese de que se cumplan por lo menos dos entregas", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        let suscripcion = Suscripcion(fechaInicio: Date(), fechaFin: self.fechaFinPicker.date, fechaAngendada: fechas!, QR: "", farmacia: self.farmacia!, medicamentos: medsAPedir, ordenes: [self.orden!])
        
        SuscripcionPersistence.postSuscripcion(suscripcion: suscripcion) { (exitoso, err) in
            if exitoso {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuscripcionListUpdate"), object: nil)
            } else {
                
            }
        }
        
        
        
        
        for frecuencia in orden!.frecuencias {
            medsAPedir.append(frecuencia.medicamento)
        }
        
        
        if !LoginViewController.conn! {
            let alert = UIAlertController(title: "No hay conexión", message: "En el momento en que se recupere conexión, se registrará el cambio", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                (alert: UIAlertAction) -> Void in
                self.performSegueToReturnBack()
            }))
            
            self.present(alert, animated: true)
        } else {performSegueToReturnBack()}
    }
    
    @IBAction func cancelarButton(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var fechaFinPicker: UIDatePicker!
    @IBOutlet weak var semanaPicker: UIPickerView!
    @IBOutlet weak var frecuenciaPicker: UIPickerView!
    @IBOutlet weak var cantidadPeriodoTextField: UITextField!
    
    let locationManager = CLLocationManager()
    
    var suscripcion: Suscripcion?
    var farmacias = [Farmacia]()
    var farmacia: Farmacia?
    var orden: OrdenMedica?
    
    var pickerData2 = [String]()
    
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        cantidadPeriodoTextField.delegate = self
        
        fechaFinPicker.locale = Locale(identifier: "es_ES")
        dateFormatter.locale = Locale(identifier: "es_ES")
        
        map.delegate = self
        setupUI()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        loadFarmacias()
        if let initialLocation = locationManager.location{
            centerMapOnLocation(location: initialLocation)
        }
        map.showsUserLocation = true
        
        semanaPicker.delegate = self
        semanaPicker.dataSource = self
        
        frecuenciaPicker.delegate = self
        frecuenciaPicker.dataSource = self
        
        pickerData2 = ["Semanas", "Meses"]
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    
    var label: UILabel?
    
    @objc func showToastNoConn() {
        self.UI {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    @objc func showToastConn() {
        self.UI {
            if self.label != nil {
                self.hideToast(toast: self.label!)
            }
            
            self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
            self.label = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !LoginViewController.conn! && self.label==nil  {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 2
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return dateFormatter.weekdaySymbols.count
        }
        else {
            return pickerData2.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return dateFormatter.weekdaySymbols[row]
        }
        else {
            return pickerData2[row]
        }
    }
    
    
    
    func setupUI(){
        
        let _annotation = FarmaciaMarker(farmacia: Farmacia(coordinate: CLLocationCoordinate2D(latitude: 1.2835921, longitude: 103.8448966), direccion: "Carrera 13", telefono: "h", nombre: "Cruz verde", horario: "h"))
        map.addAnnotation(_annotation)
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        map.setRegion(coordinateRegion, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "Capital"
        
        if annotation is FarmaciaMarker {
            if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) {
                annotationView.annotation = annotation
                return annotationView
            } else {
                let annotationView = MKPinAnnotationView(annotation:annotation, reuseIdentifier:identifier)
                annotationView.isEnabled = true
                annotationView.canShowCallout = true
                
                let btn = UIButton(type: .detailDisclosure)
                btn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
                annotationView.rightCalloutAccessoryView = btn
                return annotationView
            }
        }
        
        return nil
    }
    
    @objc func buttonAction(sender: UIButton!){
        
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let capital = view.annotation as! FarmaciaMarker
        let placeName = capital.title
        let placeInfo = "capital.info"
        let ac = UIAlertController(title: placeName, message: placeInfo, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
    
    func loadFarmacias () {
        FarmaciaPersistence.getFarmacias(){(lista, error) in
            self.farmacias = lista
            for farmacia in self.farmacias {
                self.map.addAnnotation(FarmaciaMarker(farmacia: farmacia))
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let far = view.annotation as! FarmaciaMarker
        farmacia = far.farmacia
    }
    
}

extension OrdenSuscripcionViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotation? {
        guard annotation is FarmaciaMarker else {
            return nil
        }
        let _identifier = "marker"
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: _identifier) as? MKMarkerAnnotationView
        {
            dequeuedView.annotation = annotation
            view = dequeuedView
        }
        else{
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: _identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view as? MKAnnotation
    }
}
