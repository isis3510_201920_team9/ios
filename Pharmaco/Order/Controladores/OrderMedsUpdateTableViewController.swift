//
//  OrderMedsUpdateTableViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/10/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class OrderMedsUpdateTableViewController: UITableViewController {
    
    var order: OrdenMedica?
    var frecSelected: Frecuencia?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadList), name: NSNotification.Name(rawValue: "orderAddMedUpdateReload"), object: nil)
    }
    
    @objc func reloadList() {
        self.tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order!.frecuencias.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "orderMedsUpdateIdentifier", for: indexPath) as? OrderMedsUpdateTableViewCell else {
            fatalError("No se pudo realizar el downcast")
        }
        
        let med = order?.frecuencias[indexPath.row].medicamento
        // Configure the cell...
        cell.nombreLabel.text = med?.nombre
        cell.laboratorioLabel.text = med?.nombreLab
        cell.cantidadLabel.text = String(med!.unidadesPedidas)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView,
                            trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        // Write action code for the trash
        let TrashAction = UIContextualAction(style: .normal, title:  "Eliminar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            let alertMenu = UIAlertController(title: "Eliminar", message: "¿Eliminar orden?", preferredStyle: .alert)
            
            let confirmar = UIAlertAction(title: "Confirmar", style: .destructive, handler: {
                (alert: UIAlertAction) -> Void in
                //TODO
                self.order?.frecuencias.remove(at: indexPath.row)
            })
            let cancelar = UIAlertAction(title: "Cancelar", style: .default)
            
            alertMenu.addAction(confirmar)
            alertMenu.addAction(cancelar)
            self.present(alertMenu, animated: true, completion: nil)
            success(true)
        })
        TrashAction.backgroundColor = .red
        
        let MoreAction = UIContextualAction(style: .normal, title:  "Editar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            self.frecSelected = self.order?.frecuencias[indexPath.row]
            self.performSegue(withIdentifier: "editarMed", sender: nil)
            success(true)
        })
        MoreAction.backgroundColor = .gray
        
        
        return UISwipeActionsConfiguration(actions: [TrashAction,MoreAction])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editarMed" {
            let vc = segue.destination as? OrderUpdateMedViewController
            vc!.frec = frecSelected
        }
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
