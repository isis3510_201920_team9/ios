//
//  OrderMedsTableViewCell.swift
//  Pharmaco
//
//  Created by Admin on 10/4/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class OrderMedsTableViewCell: UITableViewCell {

    @IBOutlet weak var medNameLabel: UILabel!
    @IBOutlet weak var labLabel: UILabel!
    @IBOutlet weak var cantidadLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
