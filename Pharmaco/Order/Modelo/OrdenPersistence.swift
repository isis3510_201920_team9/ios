//
//  OrdenPersistence.swift
//  Pharmaco
//
//  Created by Nicolas Caceres on 10/4/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation

import Firebase

class OrdenPersistence
{
    static private let db = Firestore.firestore()
    
    static func getOrdenes (completion: @escaping ([OrdenMedica], Error?) -> Void) {
        var lista = [OrdenMedica]()
        db.collection("/clientes/\(Autenticar.getUser().uid)/ordenesMedicas").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                
            } else {
                for document in querySnapshot!.documents {
                    //print("\(document.documentID) => \(document.data())")
                    let doctor = document.get("doctor") as! String
                    let fechaSeg = document.get("fecha") as! Timestamp
                    let fecha = Date(timeIntervalSince1970: TimeInterval(fechaSeg.seconds)-18000)
                    let tipo = document.get("tipo") as! String
                    let audioOpc = document.get("audio") as? String
                    let fotoOpc  = document.get("foto") as? String
                    let audio = audioOpc ?? ""
                    let foto = fotoOpc ?? ""
                    let especialidad = document.get("especialidad") as! String
                    let borradaTemp = document.get("borrada") as? Bool
                    let borrada = borradaTemp ?? false
                    let idOrden = document.documentID
                    self.transformarFrecuencias(idOrdenMedica: idOrden){(frecuencias,error) in
                        let suscripciones = [Suscripcion]()
                        let domicilios = [Domicilio]()
                        let turnos = [Turno]()
                        let ordenActual = OrdenMedica(especialidad:especialidad, borrada: borrada, doctor: doctor, fecha: fecha, tipo: tipo, audio: audio, foto: foto, id: idOrden, suscripciones: suscripciones, domicilios: domicilios, turnos: turnos, frecuencias: frecuencias)
                        lista.append(ordenActual)
                        if(lista.count == querySnapshot!.documents.count)
                        {
                            completion(lista, nil)
                        }
                    }
                }
            }
        }
    }
    
    static func getOrdenesVigentes (completion: @escaping ([OrdenMedica], Error?) -> Void) {
        var lista = [OrdenMedica]()
        db.collection("/clientes/\(Autenticar.getUser().uid)/ordenesMedicas").whereField("borrada", isEqualTo: false).order(by: "fecha", descending: true).getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                
            } else {
                for document in querySnapshot!.documents {
                    //print("\(document.documentID) => \(document.data())")
                    let doctor = document.get("doctor") as! String
                    let fechaSeg = document.get("fecha") as! Timestamp
                    let fecha = Date(timeIntervalSince1970: TimeInterval(fechaSeg.seconds)-18000)
                    let tipo = document.get("tipo") as! String
                    let audioOpc = document.get("audio") as? String
                    let fotoOpc  = document.get("foto") as? String
                    let audio = audioOpc ?? ""
                    let foto = fotoOpc ?? ""
                    let especialidad = document.get("especialidad") as! String
                    let borradaTemp = document.get("borrada") as? Bool
                    let borrada = borradaTemp ?? false
                    let idOrden = document.documentID
                    self.transformarFrecuencias(idOrdenMedica: idOrden){(frecuencias,error) in
                        let suscripciones = [Suscripcion]()
                        let domicilios = [Domicilio]()
                        let turnos = [Turno]()
                        let ordenActual = OrdenMedica(especialidad:especialidad, borrada: borrada, doctor: doctor, fecha: fecha, tipo: tipo, audio: audio, foto: foto, id: idOrden, suscripciones: suscripciones, domicilios: domicilios, turnos: turnos, frecuencias: frecuencias)
                        lista.append(ordenActual)
                        if(lista.count == querySnapshot!.documents.count)
                        {
                            completion(lista, nil)
                        }
                    }
                }
            }
        }
    }
    
    static func transformarFrecuencias(idOrdenMedica: String, completion: @escaping (Array<Frecuencia>, Error?) -> Void){
        var lista = Array<Frecuencia>()
        db.collection("/clientes/\(Autenticar.getUser().uid)/ordenesMedicas/"+idOrdenMedica+"/frecuencias").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                completion(lista, nil)
                
            } else {
                for document in querySnapshot!.documents {
                    let unidades = document.get("unidades") as! Int
                    let numero = document.get("numero") as! Int
                    let unidadFrecuencia = document.get("unidadFrecuencia") as! String
                    let periodo = document.get("periodo") as! Int
                    let unidadPeriodo = document.get("unidadPeriodo") as! String
                    let medicamento = document.get("medicamento") as! Dictionary<String,Any>
                    let nombre = medicamento["nombre"] as! String
                    let nombreLab = medicamento["nombreLab"] as! String
                    let dosisNumero = medicamento["dosisNumero"] as! Int
                    let dosisUnidad = medicamento["dosisUnidad"] as! String
                    let dosisCaja = 0
                    let medicamentoObj = Medicamento(nombre: nombre, nombreLab: nombreLab, dosisNumero: dosisNumero, dosisUnidad: dosisUnidad, unidadesPedidas: 0, dosisCaja: dosisCaja)
                    let frecuenciaActual = Frecuencia(unidades: unidades, numero: numero, unidadFrecuencia: unidadFrecuencia, periodo: periodo, unidadPeriodo: unidadPeriodo,medicamento: medicamentoObj)
                    lista.append(frecuenciaActual)
                    
                    //print("\(document.documentID) => \(document.data())")
                }
                completion(lista, nil)
            }
        }
    }
    static func postOrdenMedica (ordenMedica: OrdenMedica, completion: @escaping (Bool, Error?) -> Void) {
        var ref: DocumentReference? = nil
        var datos = [String:Any?]()
        datos["doctor"] = ordenMedica.doctor
        datos["fecha"] = Date(timeIntervalSince1970:ordenMedica.fecha.timeIntervalSince1970+18000)
        datos["tipo"] = ordenMedica.tipo
        datos["especialidad"] = ordenMedica.especialidad
        datos["borrada"] = ordenMedica.borrada
        if(ordenMedica.audio != ""){
            datos.updateValue(ordenMedica.audio, forKey: "audio")
        }
        else{
            datos.updateValue(nil, forKey: "audio")
        }
        if(ordenMedica.foto != ""){
            datos.updateValue(ordenMedica.foto, forKey: "foto")
        }
        else{
            datos.updateValue(nil, forKey: "foto")
        }
        datos.updateValue(nil, forKey: "suscripciones")
        datos.updateValue(nil, forKey: "domicilios")
        datos.updateValue(nil, forKey: "turnos")
        ref = db.collection("/clientes/\(Autenticar.getUser().uid)/ordenesMedicas").addDocument(data: datos) { err in
            if let err = err {
                print("Error adding document: \(err)")
                completion(false,err)
                
            } else {
                print("Document added with ID: \(ref!.documentID)")
                self.agregarFrecuencias(id: ref!.documentID, frecuencias: ordenMedica.frecuencias){ (exitoso, error) in
                    if(exitoso){
                        completion(true,nil)
                    }
                    else{
                        completion(false,error)
                    }
                    
                }
            }
        }
    }
    
    static func agregarFrecuencias(id: String, frecuencias: Array<Frecuencia>, completion: @escaping (Bool, Error?) -> Void){
        var ref: DocumentReference? = nil
        var contador = frecuencias.count
        for frecuencia in frecuencias{
            var datos = [String:Any?]()
            datos["unidades"] = frecuencia.unidades
            datos["unidadPeriodo"] = frecuencia.unidadPeriodo
            datos["numero"] = frecuencia.numero
            datos["periodo"] = frecuencia.periodo
            datos["unidadFrecuencia"] = frecuencia.unidadFrecuencia
            var medicamentoActual = Dictionary<String,Any>()
            medicamentoActual["nombre"] = frecuencia.medicamento.nombre
            medicamentoActual["nombreLab"] = frecuencia.medicamento.nombreLab
            medicamentoActual["dosisNumero"] = frecuencia.medicamento.dosisNumero
            medicamentoActual["dosisUnidad"] = frecuencia.medicamento.dosisUnidad
            datos["medicamento"] = medicamentoActual
            contador-=1;
            ref = db.collection("/clientes/\(Autenticar.getUser().uid)/ordenesMedicas/"+id+"/frecuencias").addDocument(data: datos) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                    completion(false, err)
                    
                } else {
                    print("Document added with ID: \(ref!.documentID)")
                    if(contador == 0)
                    {
                        completion(true, nil)
                    }
                }
            }
        }
    }
    static func updateOrdenMedica(ordenMedica: OrdenMedica, completion: @escaping (Bool, Error?) -> Void){
        if(ordenMedica.suscripciones.count == 0 && ordenMedica.domicilios.count == 0 && ordenMedica.turnos.count == 0){
            self.postOrdenMedica (ordenMedica: ordenMedica){(exitoso,error) in
                if(exitoso)
                {
                    self.db.collection("/clientes/\(Autenticar.getUser().uid)/ordenesMedicas/"+ordenMedica.id+"/frecuencias").getDocuments() { (querySnapshot, err) in
                        if let err = err {
                            print("Error getting documents: \(err)")
                            completion(false, err)
                            
                        } else {
                            var contador = querySnapshot!.documents.count
                            for document in querySnapshot!.documents {
                                document.reference.delete()
                                contador -= 1
                                if(contador == 0)
                                {
                                    self.db.document("/clientes/\(Autenticar.getUser().uid)/ordenesMedicas/"+ordenMedica.id).delete() { err in
                                        if let err = err {
                                            print("Error removing document: \(err)")
                                            completion(false, err)
                                        } else {
                                            print("Document successfully removed!")
                                            completion(true, nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    completion(false, error)
                }
            }
        }
        else {
            //No se puede actualizar en caso de tener turnos domicilios o subs asociadas.
            completion(false, nil)
        }
    }
    static func anadirPedidoAOrden(tipo:String, ordenMedica: DocumentReference, ref: DocumentReference, completion: @escaping (Bool, Error?) -> Void){
        ordenMedica.getDocument(){ (document, error) in
            if let document = document, document.exists {
                var listaOrdenes = document.get(tipo) as? Array<DocumentReference>
                if(listaOrdenes != nil)
                {
                    listaOrdenes!.append(ref)
                }
                else{
                    listaOrdenes = Array<DocumentReference>()
                    listaOrdenes!.append(ref)
                }
                ordenMedica.updateData([
                    tipo: listaOrdenes!
                ]) { err in
                    if let err = err {
                        print("Error updating document: \(err)")
                        completion(false, err)
                    } else {
                        print("Document successfully updated")
                        completion(true, nil)
                    }
                }
            } else {
                print("Document does not exist")
                completion(false, error)
            }
        }
    }
    
    static func deleteOrden(ordenId: String, completion: @escaping (Bool, Error?) -> Void){
        let turno: DocumentReference = db.document("/clientes/\(Autenticar.getUser().uid)/ordenesMedicas/"+ordenId);
        
        turno.updateData([
            "borrada": true,
        ]) { err in
            if let err = err {
                print("Error updating document: \(err)")
                completion(false, err)
            } else {
                print("Document successfully updated/cancelled")
                completion(true, nil)
            }
        }
        
    }
    
    static func getMedicamentos(completion: @escaping (Array<Medicamento>, Array<OrdenMedica>, Error?) -> Void){
        getOrdenesVigentes(){(ordenes,error) in
            var medicamentos = Array<Medicamento>()
            var ordenesResp = Array<OrdenMedica>()
            if(error == nil){
                for orden in ordenes{
                    for frecuencia in orden.frecuencias {
                        medicamentos.append(frecuencia.medicamento)
                        ordenesResp.append(orden)
                    }
                }
                completion(medicamentos,ordenesResp,nil)
            }
            else{
                completion(medicamentos,ordenesResp,error)
            }
            
        }
    }
    static func getMedicamentos(ordenes: Array<OrdenMedica>, completion: @escaping (Array<Medicamento>, Array<Int>, Error?) -> Void){
        getOrdenesVigentes(){(ordenes,error) in
            var medicamentos = Array<Medicamento>()
            var indices = Array<Int>()
            if(error == nil){
                var contador = 0
                for orden in ordenes{
                    for frecuencia in orden.frecuencias {
                        medicamentos.append(frecuencia.medicamento)
                        indices.append(contador)
                    }
                    contador += 1
                }
                completion(medicamentos,indices,nil)
            }
            else{
                completion(medicamentos,indices,error)
            }
            
        }
    }
}
