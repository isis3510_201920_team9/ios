//
//  Orden.swift
//  Pharmaco
//
//  Created by Admin on 10/2/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class OrdenMedica: Equatable {
    static func == (lhs: OrdenMedica, rhs: OrdenMedica) -> Bool {
        lhs.id == rhs.id
    }
    
    var id: String
    var doctor: String
    var especialidad: String
    var fecha: Date
    var tipo: String
    var audio: String
    var foto: String
    var borrada: Bool
    var suscripciones: [Suscripcion]
    var domicilios: [Domicilio]
    var turnos: [Turno]
    var frecuencias: [Frecuencia]

    
    init(especialidad:String, borrada: Bool, doctor: String, fecha: Date, tipo: String, audio: String, foto: String, id: String, suscripciones: [Suscripcion], domicilios: [Domicilio], turnos: [Turno], frecuencias: [Frecuencia]) {
        self.doctor = doctor
        self.especialidad = especialidad
        self.fecha = fecha
        self.tipo = tipo
        self.audio = audio
        self.foto = foto
        self.id = id
        self.borrada = borrada
        self.suscripciones = suscripciones
        self.domicilios = domicilios
        self.turnos = turnos
        self.frecuencias = frecuencias
    }
    //Constructor para suscripcion, turno y domicilio
    init(especialidad:String, doctor: String, fecha: Date, tipo: String, audio: String, foto: String, id: String) {
        self.especialidad = especialidad
        self.borrada = false
        self.doctor = doctor
        self.fecha = fecha
        self.tipo = tipo
        self.audio = audio
        self.foto = foto
        self.id = id
        self.suscripciones = [Suscripcion]()
        self.domicilios = [Domicilio]()
        self.turnos = [Turno]()
        self.frecuencias = [Frecuencia]()
    }
    //Constructor para el front
    init(especialidad:String, doctor: String, fecha: Date, tipo: String, audio: String, foto: String, frecuencias: [Frecuencia]) {
        self.doctor = doctor
        self.especialidad = especialidad
        self.fecha = fecha
        self.tipo = tipo
        self.audio = audio
        self.foto = foto
        self.id = ""
        self.borrada = false
        self.suscripciones = [Suscripcion]()
        self.domicilios = [Domicilio]()
        self.turnos = [Turno]()
        self.frecuencias = frecuencias
    }
}

