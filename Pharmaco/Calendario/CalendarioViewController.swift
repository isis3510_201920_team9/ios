//
//  CalendarioViewController.swift
//  Pharmaco
//
//  Created by Admin on 11/6/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class CalendarioViewController: UIViewController {
    @IBAction func crearButton(_ sender: UIButton) {
        let optionMenu = UIAlertController(title: nil, message: "Selecciona una opción", preferredStyle: .actionSheet)
              
              let agregarOrdenAction = UIAlertAction(title: "Agregar una orden", style: .default, handler: {
                  (alert: UIAlertAction) -> Void in
                  self.performSegue(withIdentifier: "nuevaOrdenSegue", sender: nil)
                  print(MedicamentoPersistence.query(nombreMed: "DOL"))
              })
              let pedirTurnoAction = UIAlertAction(title: "Pedir un turno", style: .default, handler: {
                  (alert: UIAlertAction) -> Void in
                  self.performSegue(withIdentifier: "turnoCreateSegue", sender: nil)
              })
              let pedirDomicilioAction = UIAlertAction(title: "Pedir un domicilio", style: .default, handler: {
                  (alert: UIAlertAction) -> Void in
                  self.performSegue(withIdentifier: "createDomicilioSegue", sender: nil)
              })
              let suscribirseFarmaciaAction = UIAlertAction(title: "Suscribirse a farmacia", style: .default, handler: {
                  (alert: UIAlertAction) -> Void in
                  self.performSegue(withIdentifier: "SuscribirseFarmaciaSegue", sender: nil)
              })
              
              let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel)
              
              optionMenu.addAction(agregarOrdenAction)
              optionMenu.addAction(pedirTurnoAction)
              optionMenu.addAction(pedirDomicilioAction)
              optionMenu.addAction(suscribirseFarmaciaAction)
              optionMenu.addAction(cancelAction)
              
              self.present(optionMenu, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    
    var label: UILabel?
    
    @objc func showToastNoConn() {
        self.UI {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    @objc func showToastConn() {
        self.UI {
            if self.label != nil {
                self.hideToast(toast: self.label!)
            }
            
            self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
            self.label = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !LoginViewController.conn! && self.label==nil  {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
}
