//
//  PerfilViewController.swift
//  Pharmaco
//
//  Created by Admin on 11/10/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class PerfilViewController: UIViewController {
    
    let defaults = UserDefaults.standard

    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var epsLabel: UILabel!
    @IBOutlet weak var identificacionLabel: UILabel!
    @IBOutlet weak var correoLabel: UILabel!
    
    @IBAction func editarButton(_ sender: UIButton) {
    }
    @IBAction func cambiarContrasenhaButton(_ sender: UIButton) {
        if !LoginViewController.conn! {
            let alert = UIAlertController(title: "Error de conexión", message: "No se puede reestablecer tu contraseña en este momento, revisa tu conexión e intenta de nuevo", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true)
        } else {
            Autenticar.forgotPassword()
            let alert = UIAlertController(title: "Restablecer contraseña", message: "Ha sido enviado un correo para poder reestablecer tu contraseña", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
        }
    }
    @IBAction func logoutButton(_ sender: UIButton) {
        Autenticar.logout()
        
        performSegue(withIdentifier: "logout", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NSNotification.Name(rawValue: "reloadPerfil"), object: nil)
        
        self.nombreLabel.text = (self.defaults.string(forKey: "nombre")!)
        self.epsLabel.text = (self.defaults.string(forKey: "eps")!)
        self.identificacionLabel.text = (self.defaults.string(forKey: "numDocumento")!)
        self.correoLabel.text = (self.defaults.string(forKey: "correo")!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
        }
        
        var label: UILabel?
        
        @objc func showToastNoConn() {
            self.UI {
                self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
            }
        }
        
        @objc func showToastConn() {
            self.UI {
                if self.label != nil {
                    self.hideToast(toast: self.label!)
                }
                
                self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
                self.label = nil
            }
        }
        
        override func viewDidAppear(_ animated: Bool) {
            if !LoginViewController.conn! && self.label==nil  {
                self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
            }
        }
    
    @objc func reloadData() {
        self.nombreLabel.text = (self.defaults.string(forKey: "nombre")!)
        self.epsLabel.text = (self.defaults.string(forKey: "eps")!)
        self.identificacionLabel.text = (self.defaults.string(forKey: "numDocumento")!)
        self.correoLabel.text = (self.defaults.string(forKey: "correo")!)
    }
}
