//
//  ActualizarPerfilViewController.swift
//  Pharmaco
//
//  Created by Admin on 11/20/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class ActualizarPerfilViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var epsTextField: UITextField!
    @IBOutlet weak var correoTextField: UITextField!
    @IBOutlet weak var identificacionPicker: UIPickerView!
    @IBOutlet weak var identificacionTextField: UITextField!
    @IBOutlet weak var generoPicker: UIPickerView!
    @IBOutlet weak var nacimientoPicker: UIDatePicker!
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        
        generoPicker.delegate = self
        identificacionPicker.delegate = self
        nombreTextField.delegate = self
        epsTextField.delegate = self
        
        nacimientoPicker.maximumDate = Date()
        nacimientoPicker.locale = Locale(identifier: "es-ES")
        
        self.nombreTextField.insertText(self.defaults.string(forKey: "nombre")!)
        self.epsTextField.insertText(self.defaults.string(forKey: "eps")!)
        self.correoTextField.insertText(self.defaults.string(forKey: "correo")!)
        self.identificacionTextField.insertText(self.defaults.string(forKey: "numDocumento")!)
        self.nacimientoPicker.date = self.defaults.object(forKey: "fechaNacimiento") as! Date
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    
    var label: UILabel?
    
    @objc func showToastNoConn() {
        self.UI {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    @objc func showToastConn() {
        self.UI {
            if self.label != nil {
                self.hideToast(toast: self.label!)
            }
            
            self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
            self.label = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !LoginViewController.conn! && self.label==nil  {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 40
    }
    
    @IBAction func guardarButton(_ sender: UIButton) {
        
        if nombreTextField.text!.count == 0 {
            nombreTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            nombreTextField.layer.borderWidth = 1
            nombreTextField.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if correoTextField.text!.count == 0 {
            correoTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            correoTextField.layer.borderWidth = 1
            correoTextField.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if epsTextField.text!.count == 0 {
            epsTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            epsTextField.layer.borderWidth = 1
            epsTextField.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if identificacionTextField.text!.count == 0 {
            identificacionTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            identificacionTextField.layer.borderWidth = 1
            identificacionTextField.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        let usuario = Usuario(id: Autenticar.getUser().uid, email: self.correoTextField.text!, eps: self.epsTextField.text!,
                              fechaNacimiento: self.nacimientoPicker.date, genero: self.pickerViewData2[self.generoPicker.selectedRow(inComponent: 0)], nombre:
            self.nombreTextField.text!, numDocumento: self.identificacionTextField.text!, tipoDocumento:
            self.pickerViewData[self.identificacionPicker.selectedRow(inComponent: 0)])
        
        self.defaults.set(usuario.email, forKey: "correo")
        self.defaults.set(usuario.eps, forKey: "eps")
        self.defaults.set(usuario.fechaNacimiento, forKey: "fechaNacimiento")
        self.defaults.set(usuario.nombre, forKey: "nombre")
        
        if !LoginViewController.conn! {
            let alert = UIAlertController(title: "No hay conexión", message: "Se enviará el cambio apenas se recupere la conexión", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                (alert: UIAlertAction) -> Void in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadPerfil"), object: nil)
                self.performSegueToReturnBack()
            }))
            
            self.present(alert, animated: true)
        }
        
        Autenticar.getUser().updateEmail(to: correoTextField.text!, completion:{ error in
            if let _ = error {
                let alert = UIAlertController(title: "Hubo un error", message: "No fue posible actualizar la información", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                
                self.present(alert, animated: true)
            } else {
                UsuarioPersistence.postUsuario(usuario: usuario) { (exitoso, err) in
                    if let _ = error {
                        let alert = UIAlertController(title: "Hubo un error", message: "No fue posible actualizar la información", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        
                        self.present(alert, animated: true)
                    } else {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadPerfil"), object: nil)
                    }
                }
            }
        })
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadPerfil"), object: nil)
        self.performSegueToReturnBack()
    }
    
    @IBAction func cancelarButton(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    
    var pickerViewData = ["Masculino", "Femenino", "Otro"]
    var pickerViewData2 = ["CC", "TI"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return pickerViewData2.count
        }
        return pickerViewData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0{
            return pickerViewData2[row]
        }
        return pickerViewData[row]
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
