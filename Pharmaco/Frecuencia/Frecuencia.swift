//
//  Frecuencia.swift
//  Pharmaco
//
//  Created by Admin on 10/3/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation

class Frecuencia {
    var unidades: Int
    var numero: Int
    var unidadFrecuencia: String
    var periodo: Int
    var unidadPeriodo: String
    var medicamento: Medicamento
    
    init(unidades: Int, numero: Int, unidadFrecuencia: String, periodo: Int, unidadPeriodo: String, medicamento: Medicamento) {
        self.unidades = unidades
        self.numero = numero
        self.unidadFrecuencia = unidadFrecuencia
        self.periodo = periodo
        self.unidadPeriodo = unidadPeriodo
        self.medicamento = medicamento
    }
}
