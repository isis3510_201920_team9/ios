//
//  FarnaciaPersistence.swift
//  Pharmaco
//
//  Created by Nicolas Caceres on 8/10/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation
import Firebase
import MapKit

class FarmaciaPersistence
{
    static private let db = Firestore.firestore()
    
    static func getFarmacias (completion: @escaping ([Farmacia], Error?) -> Void) {
        var lista = [Farmacia]()
        db.collection("/farmacias/").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                completion(lista, err)
                
            } else {
                for document in querySnapshot!.documents {
                    //print("\(document.documentID) => \(document.data())")
                    
                    let direccion = document.get("direccion") as! String
                    let horario = document.get("horario") as! String
                    let nombre = document.get("nombre") as! String
                    let telefono = document.get("telefono") as! String
                    let coordenadas = document.get("ubicacion") as! GeoPoint
                    let coordinate = CLLocationCoordinate2D(latitude: coordenadas.latitude, longitude: coordenadas.longitude)
                    lista.append(Farmacia(coordinate: coordinate, direccion: direccion, telefono: telefono, nombre: nombre, horario: horario))
                    if(lista.count == querySnapshot!.documents.count)
                    {
                        completion(lista, nil)
                    }
                }
            }
        }
    }
    
}
