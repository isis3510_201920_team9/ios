//
//  FarmaciaMarker.swift
//  Pharmaco
//
//  Created by Admin on 10/5/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation
import MapKit

class FarmaciaMarker: NSObject, MKAnnotation {
    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    var farmacia: Farmacia
    
    init(farmacia: Farmacia) {
        self.farmacia = farmacia
        self.title = farmacia.nombre
        self.coordinate = farmacia.coordinate
        self.subtitle = farmacia.direccion
    }
}
