//
//  Farmacia.swift
//  Pharmaco
//
//  Created by Admin on 10/3/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation
import MapKit

class Farmacia: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var direccion: String
    var telefono: String
    var nombre: String
    var horario: String
    
    init(coordinate: CLLocationCoordinate2D, direccion: String, telefono: String, nombre: String, horario: String) {
        self.coordinate = coordinate
        self.direccion = direccion
        self.telefono = telefono
        self.nombre = nombre
        self.horario = horario
    }
    
    init(direccion: String, telefono: String, nombre: String){
        self.coordinate = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
        self.direccion = direccion
        self.telefono = telefono
        self.nombre = nombre
        self.horario = ""
    }
}
