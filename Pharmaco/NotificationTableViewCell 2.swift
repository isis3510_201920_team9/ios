//
//  NotificationTableViewCell.swift
//  Pharmaco
//
//  Created by Admin on 10/1/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
//MARK: Properties
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var descripcionLabel: UILabel!
    
    var notifications = [Notification]()
    
    private func loadSampleNotifications(){
        let photo1 = UIImage(named: "clock.fill")
        let photo2 = UIImage(named: "clock.fill")
        let photo3 = UIImage(named: "clock.fill")

        guard let notification1 = Notification(name: "Nuevo turno agendado", foto: photo1!, descripcion: "Se ha creado un turno para mañana")
            else{
                fatalError("Unable to find picture")
        }
        
        guard let notification2 = Notification(name: "Nuevo turno agendado", foto: photo2!, descripcion: "Se ha creado un turno para mañana")
            else{
                fatalError("Unable to find picture")
        }
        
        guard let notification3 = Notification(name: "Nuevo turno agendado", foto: photo3!, descripcion: "Se ha creado un turno para mañana")
            else{
                fatalError("Unable to find picture")
        }
        
        notifications += [notification1,notification2,notification3]
    }
    
    override func viewDidLoad(){
        super.viewd
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
