//
//  RegistrarViewController.swift
//  Pharmaco
//
//  Created by Admin on 11/10/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class RegistrarViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var correoTextField: UITextField!
    @IBOutlet weak var contrasenha: UITextField!
    @IBOutlet weak var epsTextField: UITextField!
    @IBOutlet weak var tipoDocPicker: UIPickerView!
    @IBOutlet weak var cedulaTextField: UITextField!
    @IBOutlet weak var generoPicker: UIPickerView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBAction func registrarButton(_ sender: UIButton) {
        
        if !LoginViewController.conn! {
            let alert = UIAlertController(title: "No hay conexión", message: "No es posible iniciar sesión en este momento", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if nombreTextField.text!.count == 0 {
            nombreTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            nombreTextField.layer.borderWidth = 1
            nombreTextField.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if correoTextField.text!.count == 0 {
            correoTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            correoTextField.layer.borderWidth = 1
            correoTextField.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if contrasenha.text!.count == 0 {
                contrasenha.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
                contrasenha.layer.borderWidth = 1
                contrasenha.layer.cornerRadius = 5
                
                let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                
                self.present(alert, animated: true)
                return
            }
        
        if epsTextField.text!.count == 0 {
                epsTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
                epsTextField.layer.borderWidth = 1
                epsTextField.layer.cornerRadius = 5
                
                let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                
                self.present(alert, animated: true)
                return
            }
        
        if cedulaTextField.text!.count == 0 {
                cedulaTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
                cedulaTextField.layer.borderWidth = 1
                cedulaTextField.layer.cornerRadius = 5
                
                let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                
                self.present(alert, animated: true)
                return
            }
        
        Autenticar.registrar(email: correoTextField.text!, pass: contrasenha.text!, completionBlock: {(success) in
            if success {
                self.performSegueToReturnBack()
                let usuarioRegistrar = Usuario(id: Autenticar.getUser().uid, email: self.correoTextField.text!, eps: self.epsTextField.text!, fechaNacimiento: self.datePicker.date, genero: self.pickerViewData[self.generoPicker.selectedRow(inComponent: 0)], nombre: self.nombreTextField.text!, numDocumento: self.cedulaTextField.text!, tipoDocumento: self.pickerViewData2[self.tipoDocPicker.selectedRow(inComponent: 0)])
                
                UsuarioPersistence.postUsuario(usuario: usuarioRegistrar) { (success, err) in
                    if !success {
                        let alert = UIAlertController(title: "No se pudo registrar el usuario", message: "Por favor vuelve a intentarlo", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        
                        self.present(alert, animated: true)
                    }
                }
                
            } else {
                let alert = UIAlertController(title: "No se pudo registrar el usuario", message: "Ya existe un usuario con el correo dado", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                
                self.present(alert, animated: true)
            }
        })
    }
    
    @IBAction func volverButton(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    
    var pickerViewData = ["Masculino", "Femenino", "Otro"]
    var pickerViewData2 = ["CC", "TI"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        nombreTextField.delegate = self
        correoTextField.delegate = self
        epsTextField.delegate = self
        cedulaTextField.delegate = self
        
        datePicker.maximumDate = Date()
        datePicker.locale = Locale(identifier: "es-ES")
        
        self.generoPicker.delegate = self
        self.generoPicker.dataSource = self
        
        self.tipoDocPicker.delegate = self
        self.tipoDocPicker.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    
    
    var label: UILabel?
    
    @objc func showToastNoConn() {
        self.UI {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    @objc func showToastConn() {
        self.UI {
            if self.label != nil {
                self.hideToast(toast: self.label!)
            }
            
            self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
            self.label = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !LoginViewController.conn! && self.label==nil  {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    let ACCEPTABLE_CHARACTERS = "AÁBCDEÉFGHIÍJKLMNOÓPQRSTUÚVWXYZaábcdeéfghiíjklmnoópqrstuúvwxyz.1234567890@-_ "
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 35 && (string == filtered)
        
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return pickerViewData2.count
        }
        return pickerViewData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0{
            return pickerViewData2[row]
        }
        return pickerViewData[row]
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
