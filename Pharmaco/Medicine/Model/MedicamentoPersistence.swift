//
//  MedicamentoPersistence.swift
//  Pharmaco
//
//  Created by Nicolas Caceres on 14/11/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation
import SQLite3

class MedicamentoPersistence{
    static var db: OpaquePointer?
    static func openDB() {
        guard let path = Bundle.main.path(forResource: "medicamentosDB", ofType: "sqlite") else {
            fatalError("No database file found")
        }
        
        if sqlite3_open(path, &db) != SQLITE_OK {
            fatalError("error opening database")
        } else {
            print("database connection succeeded")
        }
    }
    static func query(nombreMed: String) -> [(String,String)]{
        var results = [(String,String)]()
        let nombreMedMod = "'%"+nombreMed+"%'"
        
        let queryStatementString = "SELECT * FROM medicamentosDB WHERE medicamentosDB.nombre LIKE \(nombreMedMod) GROUP BY medicamentosDB.nombre LIMIT 25;"
        var queryStatement: OpaquePointer? = nil
        // 1
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            // 2
            while (sqlite3_step(queryStatement) == SQLITE_ROW ){
                // 3
                let nombre = sqlite3_column_text(queryStatement, 1)
                
                // 4
                let laboratorio = sqlite3_column_text(queryStatement, 2)
                let nombreStr = String(cString: nombre!)
                let laboratorioStr = String(cString: laboratorio!)
                // 5
                let medActual = (nombreStr,laboratorioStr)
                results.append(medActual)
                //print("Query Result:")
                //print("\(nombreStr) | \(laboratorioStr)| \(principioActivoStr)")
                
            }
        }
        else {
            print("SELECT statement could not be prepared")
        }
        let queryStatementString2 = "SELECT * FROM medicamentosDB WHERE medicamentosDB.principioActivo LIKE \(nombreMedMod) GROUP BY medicamentosDB.nombre LIMIT 25;"
        var queryStatement2: OpaquePointer? = nil
        // 1
        if sqlite3_prepare_v2(db, queryStatementString2, -1, &queryStatement2, nil) == SQLITE_OK {
            // 2
            while (sqlite3_step(queryStatement2) == SQLITE_ROW ){
                // 3
                let nombre2 = sqlite3_column_text(queryStatement2, 1)
                
                // 4
                let laboratorio2 = sqlite3_column_text(queryStatement2, 2)
                let nombreStr2 = String(cString: nombre2!)
                let laboratorioStr2 = String(cString: laboratorio2!)
                // 5
                let medActual2 = (nombreStr2,laboratorioStr2)
                results.append(medActual2)
                //print("Query Result:")
                //print("\(nombreStr) | \(laboratorioStr)| \(principioActivoStr)")
                
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        // 6
        sqlite3_finalize(queryStatement)
        return results
    }
}


