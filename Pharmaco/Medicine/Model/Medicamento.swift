//
//  Medicine.swift
//  Pharmaco
//
//  Created by Admin on 10/2/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation

class Medicamento: Equatable {
    static func == (lhs: Medicamento, rhs: Medicamento) -> Bool {
        return lhs.nombre == rhs.nombre
    }
    
    var nombre: String
    var nombreLab: String
    var dosisNumero: Int
    var dosisUnidad: String
    var unidadesPedidas: Int
    var dosisCaja: Int
    
    init(nombre: String, nombreLab: String, dosisNumero: Int, dosisUnidad: String, unidadesPedidas: Int, dosisCaja: Int){
        self.nombre = nombre
        self.nombreLab = nombreLab
        self.dosisNumero = dosisNumero
        self.dosisUnidad = dosisUnidad
        self.unidadesPedidas = unidadesPedidas
        self.dosisCaja = dosisCaja
    }
}
