//
//  MedicamentoViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/3/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class MedicamentoViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UISearchResultsUpdating {
    
    var nombreMed: (String,String)?
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
    }
    
    @IBOutlet weak var nombreLabel: UITextField!
    @IBOutlet weak var dosisPorUnidadLabel: UITextField!
    @IBOutlet weak var cantidadUnidadLabel: UITextField!
    @IBOutlet weak var cantidadUnidadTiempoLabel: UITextField!
    @IBOutlet weak var cantidadUnidadPeriodo: UITextField!
    @IBOutlet weak var cantidadTextField: UITextField!
    @IBAction func nombreMedTextField(_ sender: UITextField) {
        self.nombreLabel.text = ""
        performSegue(withIdentifier: "BuscarMedSegue", sender: nil)
        sender.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BuscarMedSegue" {
            let vc = segue.destination as! BuscarMedViewController
            vc.parentVC = self
        }
    }
    
    weak var order: OrdenMedica?
    
    weak var tableMeds: MedicineTableViewController?
    
    @IBAction func cancelAddMedicamento(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    
    let ACCEPTABLE_CHARACTERS = "AÁBCDEÉFGHIÍJKLMNOÓPQRSTUÚVWXYZaábcdeéfghiíjklmnoópqrstuúvwxyz.1234567890 "
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
                   let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                       return false
               }
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        if textField.tag == 0 {
            return count <= 2 && (string == filtered)
        }
        else {
            return count <= 35 && (string == filtered)
        }
    }
    
    @IBAction func saveMedicamento(_ sender: UIButton) {
        
        guard let nombre = nombreLabel.text, nombreLabel.text?.count != 0 else {
            nombreLabel.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            nombreLabel.layer.borderWidth = 1
            nombreLabel.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        guard let dosisNumero = Int(dosisPorUnidadLabel.text!), dosisPorUnidadLabel.text?.count != 0 else {
            dosisPorUnidadLabel.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            dosisPorUnidadLabel.layer.borderWidth = 1
            dosisPorUnidadLabel.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        guard let unidadesPedidas = Int(cantidadUnidadLabel.text!), cantidadUnidadLabel.text?.count != 0 else {
            cantidadUnidadLabel.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            cantidadUnidadLabel.layer.borderWidth = 1
            cantidadUnidadLabel.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        
        guard let unidades = Int(cantidadTextField.text!), cantidadTextField.text?.count != 0 else {
            cantidadTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            cantidadTextField.layer.borderWidth = 1
            cantidadTextField.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        
        guard let numero = Int(cantidadUnidadTiempoLabel.text!), cantidadUnidadTiempoLabel.text?.count != 0 else {
            cantidadUnidadTiempoLabel.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            cantidadUnidadTiempoLabel.layer.borderWidth = 1
            cantidadUnidadTiempoLabel.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        guard let periodo = Int(cantidadUnidadPeriodo.text!), cantidadUnidadPeriodo.text?.count != 0 else {
            cantidadUnidadPeriodo.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            cantidadUnidadPeriodo.layer.borderWidth = 1
            cantidadUnidadPeriodo.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        let med = Medicamento(nombre: nombre, nombreLab: "MK", dosisNumero: dosisNumero, dosisUnidad: pickerData1[unidadDosisPicker.selectedRow(inComponent: 0)], unidadesPedidas: unidadesPedidas, dosisCaja: dosisNumero)
        let frecuencia = Frecuencia(unidades: unidades, numero: numero, unidadFrecuencia: pickerData2[unidadTiempoPicker.selectedRow(inComponent: 0)], periodo: periodo, unidadPeriodo: pickerData3[periodoDosisPicker.selectedRow(inComponent: 0)], medicamento: med)
        order?.frecuencias += [frecuencia]
        tableMeds!.tableView.reloadData()
        performSegueToReturnBack()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return pickerData1.count
        }
        else if pickerView.tag == 2 {
            return pickerData2.count
        }
        else{
            return pickerData3.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return pickerData1[row]
        }
        else if pickerView.tag == 2 {
            return pickerData2[row]
        }
        else{
            return pickerData3[row]
        }
    }
    
    @IBOutlet weak var unidadDosisPicker: UIPickerView!
    @IBOutlet weak var unidadTiempoPicker: UIPickerView!
    @IBOutlet weak var periodoDosisPicker: UIPickerView!
    
    
    var pickerData1: [String] = [String]()
    var pickerData2: [String] = [String]()
    var pickerData3: [String] = [String]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nombreLabel.delegate = self
        
        self.hideKeyboardWhenTappedAround()
        
        dosisPorUnidadLabel.delegate = self
        cantidadUnidadLabel.delegate = self
        cantidadUnidadTiempoLabel.delegate = self
        cantidadUnidadPeriodo.delegate = self
        cantidadTextField.delegate = self

        self.unidadDosisPicker.delegate = self
        self.unidadDosisPicker.dataSource = self
        
        self.unidadTiempoPicker.delegate = self
        self.unidadTiempoPicker.dataSource = self
        
        self.periodoDosisPicker.delegate = self
        self.periodoDosisPicker.dataSource = self
        
        pickerData1 = ["mg", "cm", "ml", "oz"]
        pickerData2 = ["horas", "dias"]
        pickerData3 = ["días", "semanas", "meses"]
        
        // Do any additional setup after loading the view.
        
        let search = UISearchController(searchResultsController: nil)
        search.searchResultsUpdater = self
        search.obscuresBackgroundDuringPresentation = false
        search.searchBar.placeholder = "Type something here to search"
        navigationItem.searchController = search
        
       NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
        }
        
        var label: UILabel?
        
        @objc func showToastNoConn() {
            self.UI {
                self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
            }
        }
        
        @objc func showToastConn() {
            self.UI {
                if self.label != nil {
                    self.hideToast(toast: self.label!)
                }
                
                self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
                self.label = nil
            }
        }
        
        override func viewDidAppear(_ animated: Bool) {
            if !LoginViewController.conn! && self.label==nil  {
                self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
            }
        }
}
