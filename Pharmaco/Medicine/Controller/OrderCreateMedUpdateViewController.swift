//
//  OrderCreateMedUpdateViewController.swift
//  Pharmaco
//
//  Created by Admin on 11/14/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class OrderCreateMedUpdateViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    var frec: Frecuencia?
    
    @IBOutlet weak var nombreMedTextField: UITextField!
    @IBOutlet weak var dosisPorUnidadTextField: UITextField!
    @IBOutlet weak var unidadDosisPicker: UIPickerView!
    @IBOutlet weak var cantidadUnidadesTextField: UITextField!
    @IBOutlet weak var tomarTextField: UITextField!
    @IBOutlet weak var cadaTextField: UITextField!
    @IBOutlet weak var cadaPicker: UIPickerView!
    @IBOutlet weak var porTextField: UITextField!
    @IBOutlet weak var porPicker: UIPickerView!
    
    var pickerData1: [String] = [String]()
    var pickerData2: [String] = [String]()
    var pickerData3: [String] = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        nombreMedTextField.delegate = self
        dosisPorUnidadTextField.delegate = self
        unidadDosisPicker.delegate = self
        cantidadUnidadesTextField.delegate = self
        tomarTextField.delegate = self
        cadaTextField.delegate = self
        porTextField.delegate = self
        
        nombreMedTextField.insertText(frec!.medicamento.nombre)
        dosisPorUnidadTextField.insertText(String(frec!.medicamento.dosisNumero))
        cantidadUnidadesTextField.insertText(String(frec!.medicamento.unidadesPedidas))
        tomarTextField.insertText(String(frec!.unidades))
        cadaTextField.insertText(String(frec!.numero))
        porTextField.insertText(String(frec!.periodo))
        
        self.unidadDosisPicker.delegate = self
        self.unidadDosisPicker.dataSource = self
        
        self.cadaPicker.delegate = self
        self.cadaPicker.dataSource = self
        
        self.porPicker.delegate = self
        self.porPicker.dataSource = self
        
        pickerData1 = ["mg", "cm", "ml", "oz"]
        pickerData2 = ["horas", "dias"]
        pickerData3 = ["días", "semanas", "meses"]
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    
    var label: UILabel?
    
    @objc func showToastNoConn() {
        self.UI {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    @objc func showToastConn() {
        self.UI {
            if self.label != nil {
                self.hideToast(toast: self.label!)
            }
            
            self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
            self.label = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !LoginViewController.conn! && self.label==nil  {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
     let ACCEPTABLE_CHARACTERS = "AÁBCDEÉFGHIÍJKLMNOÓPQRSTUÚVWXYZaábcdeéfghiíjklmnoópqrstuúvwxyz.1234567890 "
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
                   let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                       return false
               }
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        if textField.tag == 0 {
            return count <= 2 && (string == filtered)
        }
        else {
            return count <= 35 && (string == filtered)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
                   return pickerData1.count
               }
               else if pickerView.tag == 2 {
                   return pickerData2.count
               }
               else{
                   return pickerData3.count
               }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
           if pickerView.tag == 1 {
               return pickerData1[row]
           }
           else if pickerView.tag == 2 {
               return pickerData2[row]
           }
           else{
               return pickerData3[row]
           }
       }

    @IBAction func cancelButton(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    
    @IBAction func guardarButton(_ sender: UIButton) {
        frec!.medicamento.nombre = nombreMedTextField.text!
        frec!.medicamento.dosisNumero = Int(dosisPorUnidadTextField.text!)!
        frec!.medicamento.dosisUnidad = pickerData1[unidadDosisPicker.selectedRow(inComponent: 0)]
        frec!.medicamento.unidadesPedidas = Int(cantidadUnidadesTextField.text!)!
        frec!.unidades = Int(tomarTextField.text!)!
        frec!.periodo = Int(cadaTextField.text!)!
        frec!.unidadPeriodo = pickerData2[cadaPicker.selectedRow(inComponent: 0)]
        frec!.numero = Int(porTextField.text!)!
        frec!.unidadFrecuencia = pickerData3[porPicker.selectedRow(inComponent: 0)]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "orderCrearMedUpdateReload"), object: nil)
        
        performSegueToReturnBack()
    }
    
}
