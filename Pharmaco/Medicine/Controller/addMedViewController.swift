//
//  addMedViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/2/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class addMedViewController: UIViewController {
    
    weak var order: OrdenMedica?
    
    //    @IBAction func addMedButton(_ sender: UIButton) {
    //        let med = Medicamento(nombre: "Dolex", nombreLab: "MK", dosisNumero: 5, dosisUnidad: 500, unidadesPedidas: 2, dosisCaja: 10)
    //        let frecuencia = Frecuencia(numero: 5, unidadFrecuencia: "pastillas", periodo: "1 mes", medicamento: med)
    //        order?.frecuencias += [frecuencia]
    //        print(order?.frecuencias.count)
    //        if let childVC = self.children.first as? MedicineTableViewController {
    //            childVC.tableView.reloadData()
    //
    //        }
    //    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        //        if let childVC = self.children.first as? MedicineTableViewController {
        //
        //            childVC.order = order
        //            childVC.tableView.reloadData()
        //        }
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    
    var label: UILabel?
    
    @objc func showToastNoConn() {
        self.UI {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    @objc func showToastConn() {
        self.UI {
            if self.label != nil {
                self.hideToast(toast: self.label!)
            }
            
            self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
            self.label = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !LoginViewController.conn! && self.label==nil  {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "myEmbeddedSegue") {
            let childViewController = segue.destination as! MedicineTableViewController
            childViewController.order = order
            // Now you have a pointer to the child view controller.
            // You can save the reference to it, or pass data to it.
        }
        if segue.identifier == "segueAddMed" {
            let destinationVC = segue.destination as! MedicamentoViewController
            destinationVC.order = order
            let childViewController = self.children.first as! MedicineTableViewController
            destinationVC.tableMeds = childViewController
        }
    }
    
    func updateMeds(){
        let vc = self.children.first as! MedicineTableViewController
        vc.order = self.order
        vc.tableView.reloadData()
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
