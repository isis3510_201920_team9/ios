//
//  MedicineTableViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/2/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit


class MedicineTableViewController: UITableViewController {
    
    
    weak var order: OrdenMedica?
    weak var frecSelected: Frecuencia?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadList), name: NSNotification.Name(rawValue: "orderCrearMedUpdateReload"), object: nil)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//        self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @objc func reloadList() {
        self.tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order!.frecuencias.count
    }
    
    
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "MedicineTableViewCell"

        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MedicineTableViewCell else {
            fatalError("No se pudo instanciar")
        }
        
        let med = order!.frecuencias[indexPath.row].medicamento
     // Configure the cell...
        cell.nameLabel.text = "\(med.nombre) \(med.dosisNumero) \(med.dosisUnidad)"
        cell.laboratorioLabel.text = "Laboratorio: \(med.nombreLab)"
        cell.cantidadLabel.text = String(med.unidadesPedidas)
     return cell
     }
    
    override func tableView(_ tableView: UITableView,
                            trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        // Write action code for the trash
        let TrashAction = UIContextualAction(style: .normal, title:  "Eliminar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            let alertMenu = UIAlertController(title: "Eliminar", message: "¿Eliminar medicamento?", preferredStyle: .alert)
            
            let confirmar = UIAlertAction(title: "Confirmar", style: .destructive, handler: {
                (alert: UIAlertAction) -> Void in
                //TODO
                self.order?.frecuencias.remove(at: indexPath.row)
                self.tableView.reloadData()
            })
            let cancelar = UIAlertAction(title: "Cancelar", style: .default)
            
            alertMenu.addAction(confirmar)
            alertMenu.addAction(cancelar)
            self.present(alertMenu, animated: true, completion: nil)
            success(true)
        })
        TrashAction.backgroundColor = .red
        
        let MoreAction = UIContextualAction(style: .normal, title:  "Editar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            self.frecSelected = self.order?.frecuencias[indexPath.row]
            self.performSegue(withIdentifier: "updateMedSegue", sender: nil)
            success(true)
        })
        MoreAction.backgroundColor = .gray
        
        
        return UISwipeActionsConfiguration(actions: [TrashAction,MoreAction])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "updateMedSegue" {
            let vc = segue.destination as? OrderCreateMedUpdateViewController
            vc!.frec = frecSelected
        }
    }
    
}
