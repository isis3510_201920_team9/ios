//
//  SuscripcionCreateViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/9/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit
import MapKit

class SuscripcionCreateViewController: UIViewController, CLLocationManagerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var medsContainer: UIView!
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var fechaFinPicker: UIDatePicker!
    @IBOutlet weak var horaPicker: UIDatePicker!
    @IBOutlet weak var cantidadLabel: UITextField!
    @IBOutlet weak var unidadTiempoPicker: UIPickerView!
    @IBOutlet weak var diaPicker: UIPickerView!
    
    var farmacias=[Farmacia]()
    var farmacia: Farmacia?
    var medicamentos: [Medicamento]?
    var ordenes: [OrdenMedica]?
    var ordenesIncluidas = [OrdenMedica]()
    var pickerData1 = [String]()
    
    let locationManager = CLLocationManager()
    
    
    @IBAction func crearButton(_ sender: UIButton) {
        let fechaFin = fechaFinPicker.date
        let f = DateFormatter()
        let intervalo = Int(cantidadLabel.text!)
        let dia = f.weekdaySymbols.firstIndex(of: f.weekdaySymbols[diaPicker.selectedRow(inComponent: 0)])!+1
        let hora = Calendar.current.component(.hour, from: horaPicker.date)
        let minutos = Calendar.current.component(.minute, from: horaPicker.date)
        let child = self.children.first as! SuscripcionMedsCreateTableViewController
        
        medicamentos = child.medsASeleccionar
        ordenes = child.orders
        
        if farmacia == nil {
            map.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            map.layer.borderWidth = 1
            map.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor seleccione una farmcia", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if fechaFin < Date() {
            fechaFinPicker.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            fechaFinPicker.layer.borderWidth = 1
            fechaFinPicker.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor seleccione una fecha de fin válida", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if medicamentos == nil || medicamentos!.count == 0 {
            medsContainer.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            medsContainer.layer.borderWidth = 1
            medsContainer.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor seleccione uno o más medicamentos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if cantidadLabel.text?.count == 0 {
            cantidadLabel.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            cantidadLabel.layer.borderWidth = 1
            cantidadLabel.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llene todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        let fechasAgendadas = SuscripcionPersistence.crearFechasAgendadas(fechaInicio: Date(), fechaFin: fechaFin, dia: dia, intervalo: intervalo!, hora: hora, minutos: minutos)
        
        if fechasAgendadas.count < 2 {
            fechaFinPicker.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            fechaFinPicker.layer.borderWidth = 1
            fechaFinPicker.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor asegurese de que se cumplan por lo menos dos entregas", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        for orden in ordenes! {
            for frec in orden.frecuencias {
                if medicamentos!.contains(frec.medicamento) {
                    if !ordenesIncluidas.contains(orden){
                        ordenesIncluidas.append(orden)
                    }
                }
            }
        }
        
        let suscripcionNueva = Suscripcion(fechaInicio: Date(), fechaFin: fechaFin, fechaAngendada: fechasAgendadas, QR: "QR", farmacia: farmacia!, medicamentos: medicamentos!, ordenes: ordenes!)
        
        SuscripcionPersistence.postSuscripcion(suscripcion: suscripcionNueva) { (exitoso, error) in
            if exitoso {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuscripcionListUpdate"), object: nil)
            } else {
                
            }
        }
        if !LoginViewController.conn! {
            let alert = UIAlertController(title: "No hay conexión", message: "En el momento en que se recupere conexión, se registrará el cambio", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                (alert: UIAlertAction) -> Void in
                self.performSegueToReturnBack()
            }))
            
            self.present(alert, animated: true)
        } else {performSegueToReturnBack()}
    }
    
    @IBAction func cancelarButton(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        dateFormatter.locale = Locale(identifier: "es_ES")
        
        fechaFinPicker.locale = Locale(identifier: "es_ES")
        
        cantidadLabel.delegate = self
        
        fechaFinPicker.minimumDate = Date()
        
        map.delegate = self
        setupUI()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        loadFarmacias()
        if let initialLocation = locationManager.location{
            centerMapOnLocation(location: initialLocation)
        }
        map.showsUserLocation = true
            
        unidadTiempoPicker.delegate = self
        unidadTiempoPicker.dataSource = self
        pickerData1=["semana","mes"]
        
        diaPicker.delegate = self
        diaPicker.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    
       var label: UILabel?
    
    @objc func showToastNoConn() {
        self.UI {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    @objc func showToastConn() {
        self.UI {
            if self.label != nil {
                self.hideToast(toast: self.label!)
            }
            
            self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
            self.label = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !LoginViewController.conn! && self.label==nil  {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 2
    }
    
    func setupUI(){
        
        let _annotation = FarmaciaMarker(farmacia: Farmacia(coordinate: CLLocationCoordinate2D(latitude: 1.2835921, longitude: 103.8448966), direccion: "Carrera 13", telefono: "h", nombre: "Cruz verde", horario: "h"))
        map.addAnnotation(_annotation)
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        map.setRegion(coordinateRegion, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "Capital"
        
        if annotation is FarmaciaMarker {
            if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) {
                annotationView.annotation = annotation
                return annotationView
            } else {
                let annotationView = MKPinAnnotationView(annotation:annotation, reuseIdentifier:identifier)
                annotationView.isEnabled = true
                annotationView.canShowCallout = true
                
                let btn = UIButton(type: .detailDisclosure)
                btn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
                annotationView.rightCalloutAccessoryView = btn
                return annotationView
            }
        }
        
        return nil
    }
    
    @objc func buttonAction(sender: UIButton!){
        
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let capital = view.annotation as! FarmaciaMarker
        let placeName = capital.title
        let placeInfo = "capital.info"
        let ac = UIAlertController(title: placeName, message: placeInfo, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
    
    func loadFarmacias () {
        FarmaciaPersistence.getFarmacias(){(lista, error) in
            self.farmacias = lista
            for farmacia in self.farmacias {
                self.map.addAnnotation(FarmaciaMarker(farmacia: farmacia))
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let far = view.annotation as! FarmaciaMarker
        farmacia = far.farmacia
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return pickerData1.count
        } else {
            return dateFormatter.weekdaySymbols.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return pickerData1[row]
        } else {
            return dateFormatter.weekdaySymbols[row]
        }
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SuscripcionCreateViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotation? {
        guard annotation is FarmaciaMarker else {
            return nil
        }
        let _identifier = "marker"
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: _identifier) as? MKMarkerAnnotationView
        {
            dequeuedView.annotation = annotation
            view = dequeuedView
        }
        else{
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: _identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view as? MKAnnotation
    }
}
