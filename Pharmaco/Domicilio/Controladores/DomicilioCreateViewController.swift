//
//  DomicilioCreateViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/9/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit
import MapKit

class DomicilioCreateViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var medsContainer: UIView!
    
    @IBAction func crearButton(_ sender: UIButton) {
        let date = fechaPicker.date
        
        let vc = self.children.first as! DomicilioCreateMedsTableViewController
        
        let medsAPedir = vc.medsASeleccionar
        let ordenes = vc.orders
        
        var ordenesIncluidas = [OrdenMedica]()
        
        for orden in ordenes! {
            for frec in orden.frecuencias {
                if medsAPedir.contains(frec.medicamento) {
                    if !ordenesIncluidas.contains(orden){
                        ordenesIncluidas.append(orden)
                    }
                }
            }
        }
        
        if direccionLabel.text?.count == 0 {
            direccionLabel.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            direccionLabel.layer.borderWidth = 1
            direccionLabel.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llene todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if farmacia == nil {
            map.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            map.layer.borderWidth = 1
            map.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor seleccione una farmacia", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if fechaPicker.date < Date() {
            fechaPicker.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            fechaPicker.layer.borderWidth = 1
            fechaPicker.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor seleccione una fecha válida", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if medsAPedir.count == 0 {
            medsContainer.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            medsContainer.layer.borderWidth = 1
            medsContainer.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor seleccione uno o más medicamentos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        let nuevoDomicilio = Domicilio(direccion: direccionLabel.text!, observaciones: observacionesLabel.text!, fechaAgendada: date, costo: 0, QR: "QR", farmacia: farmacia!, medicamentos: medsAPedir, ordenes: ordenesIncluidas)
        
        DomicilioPersistence.postDomicilio(domicilio: nuevoDomicilio) { (exitoso: Bool, error) in
            if exitoso {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DomicilioListUpdate"), object: nil)
            }
        }
        if !LoginViewController.conn! {
            let alert = UIAlertController(title: "No hay conexión", message: "En el momento en que se recupere conexión, se registrará el cambio", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {
                (alert: UIAlertAction) -> Void in
                self.performSegueToReturnBack()
            }))
            
            self.present(alert, animated: true)
        } else {performSegueToReturnBack()}
    }
    
    @IBAction func cancelarButton(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    
    let ACCEPTABLE_CHARACTERS_LABEL = "AÁBCDEÉFGHIÍJKLMNOÓPQRSTUÚVWXYZaábcdeéfghiíjklmnoópqrstuúvwxyz()1234567890 "
    let ACCEPTABLE_CHARACTERS_DIRECCION = "AÁBCDEÉFGHIÍJKLMNOÓPQRSTUÚVWXYZaábcdeéfghiíjklmnoópqrstuúvwxyz()#-.1234567890 "
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
                   let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                       return false
               }
        
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        if textField.tag == 1 {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS_LABEL).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered) && (count <= 35)
        }
        else{
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS_DIRECCION).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered) && (count <= 35)
        }
    }
    
    
    
    @IBOutlet weak var direccionLabel: UITextField!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var fechaPicker: UIDatePicker!
    @IBOutlet weak var horaPicker: UIDatePicker!
    @IBOutlet weak var observacionesLabel: UITextField!
    
    let locationManager = CLLocationManager()
    
    var farmacia: Farmacia?
    var medicamentos = [Medicamento]()
    var ordenes = [OrdenMedica]()
    var farmacias=[Farmacia]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        fechaPicker.minimumDate = Date()
        horaPicker.minimumDate = Date()
        
        fechaPicker.locale = Locale(identifier: "es_ES")
        
        map.delegate = self
        setupUI()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        loadFarmacias()
        direccionLabel.delegate = self
        observacionesLabel.delegate = self
        if let initialLocation = locationManager.location{
            centerMapOnLocation(location: initialLocation)
        }
        map.showsUserLocation = true
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    
      var label: UILabel?
    
    @objc func showToastNoConn() {
        self.UI {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    @objc func showToastConn() {
        self.UI {
            if self.label != nil {
                self.hideToast(toast: self.label!)
            }
            
            self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
            self.label = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !LoginViewController.conn! && self.label==nil  {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    func setupUI(){
        
        let _annotation = FarmaciaMarker(farmacia: Farmacia(coordinate: CLLocationCoordinate2D(latitude: 1.2835921, longitude: 103.8448966), direccion: "Carrera 13", telefono: "h", nombre: "Cruz verde", horario: "h"))
        map.addAnnotation(_annotation)
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        map.setRegion(coordinateRegion, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "Capital"
        
        if annotation is FarmaciaMarker {
            if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) {
                annotationView.annotation = annotation
                return annotationView
            } else {
                let annotationView = MKPinAnnotationView(annotation:annotation, reuseIdentifier:identifier)
                annotationView.isEnabled = true
                annotationView.canShowCallout = true
                
                let btn = UIButton(type: .detailDisclosure)
                btn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
                annotationView.rightCalloutAccessoryView = btn
                return annotationView
            }
        }
        
        return nil
    }
    
    @objc func buttonAction(sender: UIButton!){
        
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let capital = view.annotation as! FarmaciaMarker
        let placeName = capital.title
        let placeInfo = "capital.info"
        let ac = UIAlertController(title: placeName, message: placeInfo, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
    
    func loadFarmacias () {
        FarmaciaPersistence.getFarmacias(){(lista, error) in
            self.farmacias = lista
            for farmacia in self.farmacias {
                self.map.addAnnotation(FarmaciaMarker(farmacia: farmacia))
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let far = view.annotation as! FarmaciaMarker
        farmacia = far.farmacia
    }
}

extension DomicilioCreateViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotation? {
        guard annotation is FarmaciaMarker else {
            return nil
        }
        let _identifier = "marker"
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: _identifier) as? MKMarkerAnnotationView
        {
            dequeuedView.annotation = annotation
            view = dequeuedView
        }
        else{
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: _identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view as? MKAnnotation
    }
}
