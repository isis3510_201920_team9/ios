//
//  DomicilioTableViewCell.swift
//  Pharmaco
//
//  Created by Admin on 10/6/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class DomicilioTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var fechaLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
