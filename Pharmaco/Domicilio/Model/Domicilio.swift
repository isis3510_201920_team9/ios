//
//  Domicilio.swift
//  Pharmaco
//
//  Created by Admin on 10/3/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation

class Domicilio {
    var id: String
    var direccion: String
    var observaciones: String
    var fechaAgendada: Date
    var costo: Double
    var QR: String
    var fechaEntrega: Date?
    var exitoso: Bool?
    var novedades: String?
    var farmacia: Farmacia
    var medicamentos: [Medicamento]
    var ordenes: [OrdenMedica]
    
    init(id: String, direccion: String, observaciones: String, fechaAgendada: Date, costo: Double, QR: String, fechaEntrega: Date?, existoso: Bool?, novedades: String?, farmacia: Farmacia, medicamentos: [Medicamento], ordenes: [OrdenMedica]) {
        self.id = id
        self.direccion = direccion
        self.observaciones = observaciones
        self.fechaAgendada = fechaAgendada
        self.costo = costo
        self.QR = QR
        self.fechaEntrega = fechaEntrega
        self.exitoso = existoso
        self.novedades = novedades
        self.farmacia = farmacia
        self.medicamentos = medicamentos
        self.ordenes = ordenes
    }
    //Constructor para el front
    init(direccion: String, observaciones: String, fechaAgendada: Date, costo: Double, QR: String, farmacia: Farmacia, medicamentos: [Medicamento], ordenes: [OrdenMedica]) {
        self.id = ""
        self.direccion = direccion
        self.observaciones = observaciones
        self.fechaAgendada = fechaAgendada
        self.costo = costo
        self.QR = QR
        self.fechaEntrega = nil
        self.exitoso = nil
        self.novedades = nil
        self.farmacia = farmacia
        self.medicamentos = medicamentos
        self.ordenes = ordenes
    }
    
}
