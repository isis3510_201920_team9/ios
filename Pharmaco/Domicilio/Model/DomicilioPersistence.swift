//
//  DomicilioPersistence.swift
//  Pharmaco
//
//  Created by Nicolas Caceres on 10/4/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation
import Firebase

class DomicilioPersistence
{
    static private let db = Firestore.firestore()
    
    static func getDomicilios (completion: @escaping ([Domicilio], Error?) -> Void) {
        var lista = [Domicilio]()
        db.collection("/clientes/\(Autenticar.getUser().uid)/domicilios").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                completion(lista, err)
                
            } else {
                for document in querySnapshot!.documents {
                    //print("\(document.documentID) => \(document.data())")
                    let medicamentosDict = document.get("medicamentos") as! Array<Dictionary<String,Any>>
                    let medicamentos = self.transformarMedicamentos(medicamentos: medicamentosDict)
                    let qr = document.get("qr") as! String
                    let farmaciaDict = document.get("farmacia") as! Dictionary<String,Any>
                    let farmacia = self.transformarFarmacia(farmacia: farmaciaDict)
                    let listaOrdenes = document.get("ordenes") as! Array<DocumentReference>
                    let fechaEntregaSeg = document.get("fechaEntrega") as? Timestamp
                    var fechaEntrega: Date?
                    if(fechaEntregaSeg != nil)
                    {
                        fechaEntrega = Date(timeIntervalSince1970: TimeInterval(fechaEntregaSeg!.seconds)-18000)
                    }
                    else{
                        fechaEntrega = nil
                    }
                    let fechaAgendadaSeg = document.get("fechaAgendada") as! Timestamp
                    let fechaAgendada = Date(timeIntervalSince1970: TimeInterval(fechaAgendadaSeg.seconds)-18000)
                    let direccion = document.get("direccion") as! String
                    let observaciones = document.get("observaciones") as! String
                    let costo = document.get("costo") as! Double
                    let novedades = document.get("novedades") as? String
                    let exitoso = document.get("exitoso") as? Bool
                    self.transformarOrdenes(ordenes: listaOrdenes){(ordenesTrans, error) in
                        let domicilioActual = Domicilio(id: document.documentID, direccion: direccion, observaciones: observaciones, fechaAgendada: fechaAgendada, costo: costo, QR: qr, fechaEntrega: fechaEntrega, existoso: exitoso, novedades: novedades, farmacia: farmacia, medicamentos: medicamentos, ordenes: ordenesTrans)
                        //print(suscripcionActual.ordenes)
                        lista.append(domicilioActual)
                        if(lista.count == querySnapshot!.documents.count)
                        {
                            completion(lista, nil)
                        }
                    }
                }
            }
        }
    }
    
    static func getDomiciliosVigentes (completion: @escaping ([Domicilio], Error?) -> Void) {
        var lista = [Domicilio]()
        db.collection("/clientes/\(Autenticar.getUser().uid)/domicilios").whereField("exitoso", isEqualTo: NSNull()).order(by: "fechaAgendada", descending: true).getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
                completion(lista, err)
                
            } else {
                for document in querySnapshot!.documents {
                    //print("\(document.documentID) => \(document.data())")
                    let medicamentosDict = document.get("medicamentos") as! Array<Dictionary<String,Any>>
                    let medicamentos = self.transformarMedicamentos(medicamentos: medicamentosDict)
                    let qr = document.get("qr") as! String
                    let farmaciaDict = document.get("farmacia") as! Dictionary<String,Any>
                    let farmacia = self.transformarFarmacia(farmacia: farmaciaDict)
                    let listaOrdenes = document.get("ordenes") as! Array<DocumentReference>
                    let fechaEntregaSeg = document.get("fechaEntrega") as? Timestamp
                    var fechaEntrega: Date?
                    if(fechaEntregaSeg != nil)
                    {
                        fechaEntrega = Date(timeIntervalSince1970: TimeInterval(fechaEntregaSeg!.seconds)-18000)
                    }
                    else{
                        fechaEntrega = nil
                    }
                    let fechaAgendadaSeg = document.get("fechaAgendada") as! Timestamp
                    let fechaAgendada = Date(timeIntervalSince1970: TimeInterval(fechaAgendadaSeg.seconds)-18000)
                    let direccion = document.get("direccion") as! String
                    let observaciones = document.get("observaciones") as? String
                    let costo = document.get("costo") as! Double
                    let novedades = document.get("novedades") as? String
                    let exitoso = document.get("exitoso") as? Bool
                    self.transformarOrdenes(ordenes: listaOrdenes){(ordenesTrans, error) in
                        let domicilioActual = Domicilio(id: document.documentID, direccion: direccion, observaciones: observaciones ?? "", fechaAgendada: fechaAgendada, costo: costo, QR: qr, fechaEntrega: fechaEntrega, existoso: exitoso, novedades: novedades, farmacia: farmacia, medicamentos: medicamentos, ordenes: ordenesTrans)
                        //print(suscripcionActual.ordenes)
                        lista.append(domicilioActual)
                        if(lista.count == querySnapshot!.documents.count)
                        {
                            completion(lista, nil)
                        }
                    }
                }
            }
        }
    }
    
    static func transformarOrdenes(ordenes: Array<DocumentReference>, completion: @escaping (Array<OrdenMedica>, Error?) -> Void)
    {
        var lista = Array<OrdenMedica>()
        var contador = ordenes.count
        for orden in ordenes{
            orden.getDocument(){ (document, error) in
                if let document = document, document.exists {
                    contador-=1;
                    let doctor = document.get("doctor") as! String
                    let fechaSeg = document.get("fecha") as! Timestamp
                    let fecha = Date(timeIntervalSince1970: TimeInterval(fechaSeg.seconds)-18000)
                    let tipo = document.get("tipo") as! String
                    let audioOpc = document.get("audio") as? String
                    let fotoOpc  = document.get("foto") as? String
                    let audio = audioOpc ?? ""
                    let foto = fotoOpc ?? ""
                    let idOrden = document.documentID
                    let especialidad = document.get("especialidad") as! String
                    lista.append(OrdenMedica(especialidad:especialidad, doctor: doctor, fecha: fecha, tipo: tipo, audio: audio, foto: foto, id: idOrden))
                    if(contador == 0)
                    {
                        completion(lista, nil)
                    }
                    
                } else {
                    print("Document does not exist")
                    completion(lista, nil)
                }
            }
        }
    }
    
    
    static func transformarMedicamentos(medicamentos: Array<Dictionary<String,Any>>) -> Array<Medicamento>
    {
        var lista = Array<Medicamento>()
        for medicamento in medicamentos
        {
            let nombre = medicamento["nombre"] as! String
            let nombreLab = medicamento["nombreLab"] as! String
            let dosisNumero = medicamento["dosisNumero"] as! Int
            let dosisUnidad = medicamento["dosisUnidad"] as! String
            let unidadesPedidas = medicamento["unidadesPedidas"] as! Int
            let dosisCaja = medicamento["dosisCaja"] as! Int
            lista.append(Medicamento(nombre: nombre, nombreLab: nombreLab, dosisNumero: dosisNumero, dosisUnidad: dosisUnidad, unidadesPedidas: unidadesPedidas, dosisCaja: dosisCaja))
        }
        return lista
        
    }
    static func transformarFarmacia(farmacia: Dictionary<String,Any>) -> Farmacia
    {
        let direccion = farmacia["direccion"] as! String
        let nombre = farmacia["nombre"] as! String
        let telefono = farmacia["telefono"] as! String
        return Farmacia(direccion: direccion, telefono: telefono, nombre: nombre)
        
    }
    static func postDomicilio (domicilio: Domicilio,completion: @escaping (Bool, Error?) -> Void) {
        var ref: DocumentReference? = nil
        var datos = [String:Any?]()
        datos["qr"] = domicilio.QR
        datos["direccion"] = domicilio.direccion
        datos["observaciones"] = domicilio.observaciones
        datos["fechaAgendada"] = Date(timeIntervalSince1970:domicilio.fechaAgendada.timeIntervalSince1970+18000)
        datos["costo"] = domicilio.costo
        datos["farmacia"] = ["direccion":domicilio.farmacia.direccion,"nombre":domicilio.farmacia.nombre,"telefono":domicilio.farmacia.telefono]
        var medicamentos = Array<Dictionary<String,Any>>()
        for medicamento in domicilio.medicamentos
        {
            var medicamentoActual = Dictionary<String,Any>()
            medicamentoActual["nombre"] = medicamento.nombre
            medicamentoActual["nombreLab"] = medicamento.nombreLab
            medicamentoActual["dosisNumero"] = medicamento.dosisNumero
            medicamentoActual["dosisUnidad"] = medicamento.dosisUnidad
            medicamentoActual["unidadesPedidas"] = medicamento.unidadesPedidas
            medicamentoActual["dosisCaja"] = medicamento.dosisCaja
            medicamentos.append(medicamentoActual)
        }
        datos["medicamentos"] = medicamentos
        if(domicilio.fechaEntrega != nil){
            datos["fechaEntrega"] = Date(timeIntervalSince1970:domicilio.fechaEntrega!.timeIntervalSince1970+18000)
        }
        else{
            datos.updateValue(nil, forKey: "fechaEntrega")
        }
        if(domicilio.exitoso != nil){
            datos["exitoso"] = domicilio.exitoso!
        }
        else{
            datos.updateValue(nil, forKey: "exitoso")
        }
        if(domicilio.novedades != nil){
            datos["novedades"] = domicilio.novedades!
        }
        else{
            datos.updateValue(nil, forKey: "novedades")
        }
        datos["ordenes"] = referenciarOrdenes(ordenes: domicilio.ordenes)
        //print(datos)
        ref = db.collection("/clientes/\(Autenticar.getUser().uid)/domicilios").addDocument(data: datos) { err in
            if let err = err {
                print("Error adding document: \(err)")
                completion(false, err)
            } else {
                print("Document added with ID: \(ref!.documentID)")
                self.agregarDomicilioAOrden(ordenes: datos["ordenes"] as! Array<DocumentReference>, domicilioId: ref!.documentID){ (correcto, error) in
                    if(correcto){
                        completion(true, nil)
                    }
                    else
                    {
                        completion(false, error)
                    }
                }
            }
        }
    }
    static func referenciarOrdenes(ordenes: Array<OrdenMedica>) -> Array<DocumentReference>{
        var lista = Array<DocumentReference>()
        for orden in ordenes{
            let userRef: DocumentReference = db.document("/clientes/\(Autenticar.getUser().uid)/ordenesMedicas/"+orden.id);
            lista.append(userRef)
        }
        return lista
    }
    static func agregarDomicilioAOrden(ordenes: Array<DocumentReference>, domicilioId: String, completion: @escaping (Bool, Error?) -> Void){
        let userRef: DocumentReference = db.document("/clientes/\(Autenticar.getUser().uid)/domicilios/"+domicilioId);
        var todoBien = true
        var contador = ordenes.count
        var errorGlob: Error? = nil
        for orden in ordenes{
            OrdenPersistence.anadirPedidoAOrden(tipo: "domicilios", ordenMedica: orden, ref: userRef){
                (bien,error) in
                if(!bien)
                {
                    todoBien = false
                    errorGlob = error
                }
            }
            contador -= 1
            if(contador == 0)
            {
                if(todoBien)
                {
                    completion(true, nil)
                }
                else {
                    completion(false, errorGlob)
                }
            }
        }
        
    }
    static func deleteDomicilio(domicilioId: String, completion: @escaping (Bool, Error?) -> Void){
        let domicilio: DocumentReference = db.document("/clientes/\(Autenticar.getUser().uid)/domicilios/"+domicilioId);
        
        domicilio.updateData([
            "exitoso": false,
            "novedades": "Domicilio cancelado por el usuario"
        ]) { err in
            if let err = err {
                print("Error updating document: \(err)")
                completion(false, err)
            } else {
                print("Document successfully updated")
                completion(true, nil)
            }
        }
        
    }
}
