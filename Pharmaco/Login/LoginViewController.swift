//
//  LoginViewController.swift
//  Pharmaco
//
//  Created by Admin on 11/10/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit
import LocalAuthentication
import Network

class LoginViewController: UIViewController {
    
    @IBOutlet weak var nombreUsuarioTextField: UITextField!
    
    @IBOutlet weak var contrasenhaTextField: UITextField!
    
    let defaults = UserDefaults.standard
    
    var monitor = NWPathMonitor()
    var toastNoConn: UILabel?
    static var conn: Bool?
    var label: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        navigationController?.isNavigationBarHidden = true
        
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                self.UI {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "conn"), object: nil)
                    if LoginViewController.conn != nil, !LoginViewController.self.conn!{
                        if self.label != nil {
                            self.hideToast(toast: self.label!)
                        }
                        self.label = self.showToast(message: "Hay conexión nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
                        LoginViewController.conn = true
                    }
                    LoginViewController.conn = true
                }
            } else {
                self.UI {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "noConn"), object: nil)
                    LoginViewController.conn = false
                    
                    self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
                }
            }
        }
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if Autenticar.userLoggedIn() {
            self.performSegue(withIdentifier: "InitialScreen", sender: nil)
        }
        if !LoginViewController.conn! {
            self.toastNoConn = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    @IBAction func touchID(_ sender: UIButton) {
        
        if !LoginViewController.conn! {
                   let alert = UIAlertController(title: "Error", message: "No hay conexión a internet, revisa tu conexión e inténtalo de nuevo", preferredStyle: .alert)
                   let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                   
                   alert.addAction(action)
                   self.present(alert, animated: true)
               }
        else {
        if defaults.string(forKey: "correo") == nil || defaults.string(forKey: "contrasenha") == nil || defaults.string(forKey: "uid") == nil{
            let ac = UIAlertController(title: "Error", message: "Por favor inicie sesión para habilitar el touch ID", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
            
            return
        }
        
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Indentifícate!"
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [unowned self] success, authenticationError in
                
                DispatchQueue.main.async {
                    if success {
                        Autenticar.login(email: self.defaults.string(forKey: "correo")!, pass: self.defaults.string(forKey: "contrasenha")!) {[weak self] (success,logged) in
                            guard let `self` = self else { return }
                            var message: String = ""
                            if success, !logged {
                                self.performSegue(withIdentifier: "InitialScreen", sender: nil)
                            } else if logged {
                                print(success)
                                print(logged)
                                message = "Ya tiene una sesión iniciada en otro dispositivo"
                                let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                                self.present(alertController, animated: true)
                            } else if !success {
                                message = "Nombre de usuario o contraseña incorrectos"
                                let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                                self.present(alertController, animated: true)
                            }
                        }
                        
                    }
//                    else {
//                        let ac = UIAlertController(title: "No se pudo inciar sesión", message: "Sorry!", preferredStyle: .alert)
//                        ac.addAction(UIAlertAction(title: "OK", style: .default))
//                        self.present(ac, animated: true)
//                    }
                }
            }
        } else {
            let ac = UIAlertController(title: "Touch ID not available", message: "Your device is not configured for Touch ID.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
        }
    }
    
    
    @IBAction func loginButton(_ sender: UIButton) {
        
        if !LoginViewController.conn! {
            let alert = UIAlertController(title: "No hay conexión", message: "No es posible iniciar sesión en este momento", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if nombreUsuarioTextField.text!.count == 0 {
            nombreUsuarioTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            nombreUsuarioTextField.layer.borderWidth = 1
            nombreUsuarioTextField.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        if contrasenhaTextField.text!.count == 0 {
            contrasenhaTextField.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            contrasenhaTextField.layer.borderWidth = 1
            contrasenhaTextField.layer.cornerRadius = 5
            
            let alert = UIAlertController(title: "No se pudo crear", message: "Por favor llena todos los campos", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            return
        }
        
        guard let email = nombreUsuarioTextField.text, let password = contrasenhaTextField.text else { return }
        Autenticar.login(email: email, pass: password) {[weak self] (success, logged) in
            guard let `self` = self else { return }
            var message: String = ""
            if success, !logged {
                self.performSegue(withIdentifier: "InitialScreen", sender: nil)
                let username:String = self.nombreUsuarioTextField.text!
                let password:String = self.contrasenhaTextField.text!
                let uid:String = Autenticar.getUser().uid
                self.defaults.set(username, forKey: "correo")
                self.defaults.set(password, forKey: "contrasenha")
                self.defaults.set(uid, forKey: "uid")
                UsuarioPersistence.getUsuario(id: Autenticar.getUser().uid) { (user, err) in
                    self.defaults.set(user!.eps, forKey: "eps")
                    self.defaults.set(user!.numDocumento, forKey: "numDocumento")
                    self.defaults.set(user!.nombre, forKey: "nombre")
                    self.defaults.set(user!.fechaNacimiento, forKey: "fechaNacimiento")
                }
            } else if logged {
                message = "Existe una sesión iniciada en otro dispositivo"
                let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alertController, animated: true)
            } else if !success {
                message = "Nombre de usuario o contraseña incorrectos"
                let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alertController, animated: true)
            }
        }
    }
    
    @IBAction func olvidarContrasenhaButton(_ sender: UIButton) {
        
        if !LoginViewController.conn! {
            let alert = UIAlertController(title: "Error de conexión", message: "No se puede reestablecer tu contraseña en este momento, revisa tu conexión e intenta de nuevo", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true)
        } else {
            let alert = UIAlertController(title: "Ingresa tu correo", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            alert.addTextField(configurationHandler: { textField in
                textField.placeholder = "Escribe tu correo"
            })
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
                if let email = alert.textFields?.first?.text {
                    if email.contains("@"), email.contains(".") {
                        Autenticar.forgotPasswordEmail(email: email)
                    } else {
                        let alert = UIAlertController(title: "Error", message: "Por favor ingresa un correo válido", preferredStyle: .alert)
                        
                        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alert.addAction(action)
                        
                        self.present(alert, animated: true)
                    }
                }
                
                let alert = UIAlertController(title: "Reestablecer contraseña", message: "Si el correo ingresado existe, se envió un enlace para reestablecer la contraseña", preferredStyle: .alert)
                
                let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true)
                
            }))
            
            self.present(alert, animated: true)
        }
    }
}
