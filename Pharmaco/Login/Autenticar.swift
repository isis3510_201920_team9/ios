//
//  Autenticar.swift
//  Pharmaco
//
//  Created by Admin on 11/10/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation
import FirebaseAuth

class Autenticar {
    
    static func login(email: String, pass: String, completionBlock: @escaping (_ success: Bool, _ logged: Bool) -> Void) {
        UsuarioPersistence.estaLoggeado(email: email)
        {(resp,error) in
            if(resp)
            {
                completionBlock(false,true)
            }
            else{
                Auth.auth().signIn(withEmail: email, password: pass) { (result, error) in
                    if let error = error, let _ = AuthErrorCode(rawValue: error._code) {
                        completionBlock(false,false)
                    } else {
                        UsuarioPersistence.loggearUsuario(email: email)
                        completionBlock(true,false)
                    }
                }
            }
        }
    }
    
    static func registrar(email: String, pass: String, completionBlock: @escaping (_ success: Bool) -> Void) {
        Auth.auth().createUser(withEmail: email, password: pass, completion: {(success, error) in
            if let error = error, let _ = AuthErrorCode(rawValue: error._code) {
                completionBlock(false)
            } else {
                completionBlock(true)
            }
        })
    }
    
    static func userLoggedIn()->Bool {
        return Auth.auth().currentUser != nil
        
    }
    
    static func logout(){
        do{
            UsuarioPersistence.desloggearUsuario(email:(Auth.auth().currentUser?.email!)!)
            try Auth.auth().signOut()
        } catch let err as NSError{
            print("\(err)")
        }
    }
    
    static func getUser() -> User{
        return Auth.auth().currentUser!
    }
    static func forgotPassword () {
        if (userLoggedIn())
        {
            Auth.auth().sendPasswordReset(withEmail: (Auth.auth().currentUser?.email!)!) { error in
                // Your code here
            }
        }
    }
    static func forgotPasswordEmail (email: String) {
            Auth.auth().sendPasswordReset(withEmail: email) { error in
                // Your code here

        }
    }
}
