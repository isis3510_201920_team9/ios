//
//  AddMedTableViewCell.swift
//  Pharmaco
//
//  Created by Admin on 11/16/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class AddMedTableViewCell: UITableViewCell {

    @IBOutlet weak var nombreMedLabel: UILabel!
    @IBOutlet weak var labLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
