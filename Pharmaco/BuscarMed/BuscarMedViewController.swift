//
//  BuscarMedViewController.swift
//  Pharmaco
//
//  Created by Admin on 11/16/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class BuscarMedViewController: UINavigationController {

    var parentVC: MedicamentoViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func actualizarTextField(with medicine: String) {
        parentVC!.nombreLabel.insertText(medicine)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
