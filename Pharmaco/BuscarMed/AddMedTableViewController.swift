//
//  AddMedTableViewController.swift
//  Pharmaco
//
//  Created by Admin on 11/16/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class AddMedTableViewController: UITableViewController, UITextFieldDelegate, UISearchResultsUpdating {
    
    var meds: [(String,String)]?
    
    @IBAction func volverButton(_ sender: UIBarButtonItem) {
        parent?.performSegueToReturnBack()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        let search = UISearchController(searchResultsController: nil)
        search.searchResultsUpdater = self
        search.obscuresBackgroundDuringPresentation = false
        search.searchBar.placeholder = "Escribe el nombre del medicamento"
        navigationItem.searchController = search
        
       NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
        }
        
        var label: UILabel?
        
        @objc func showToastNoConn() {
            self.UI {
                self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
            }
        }
        
        @objc func showToastConn() {
            self.UI {
                if self.label != nil {
                    self.hideToast(toast: self.label!)
                }
                
                self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
                self.label = nil
            }
        }
        
        override func viewDidAppear(_ animated: Bool) {
            if !LoginViewController.conn! && self.label==nil  {
                self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
            }
        }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meds?.count ?? 0
    }

    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text, text.count >= 3 else { return }
        meds = MedicamentoPersistence.query(nombreMed: text)
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "addMedIdentifier", for: indexPath) as? AddMedTableViewCell else {
            fatalError("No se pudo instanciar")
        }
        
        // Configure the cell...
        cell.nombreMedLabel.text = meds![indexPath.row].0
        cell.labLabel.text = meds![indexPath.row].1

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pvc = parent as! BuscarMedViewController
        pvc.actualizarTextField(with: meds![indexPath.row].0)
        parent?.performSegueToReturnBack()
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
