//
//  TurnoMedsTableViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/7/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class TurnoMedsTableViewController: UITableViewController {
    
    var ordenesP = [OrdenMedica]()
    var meds = [Medicamento]()
    var ordenes = [OrdenMedica]()
    var indices = [Int]()
    var medsAPedir = [Medicamento]()
    var farmaciaSeleccionada: Farmacia?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meds.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "medsTurnoCellIdentifier", for: indexPath) as? MedsTurnoTableViewCell else {
            fatalError("No se pudo realizar el downcast")
        }
        
        let med = meds[indexPath.row]
        // Configure the cell...
        cell.nombreLabel.text = med.nombre
        cell.laboratorioLabel.text = med.nombreLab
        cell.cantidadLabel.text = "\(med.unidadesPedidas)"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let med = meds[indexPath.row]
        if !medsAPedir.contains(med) {
            medsAPedir.append(med)
        }
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let med = meds[indexPath.row]
        medsAPedir.removeAll { (medDeseleccionado: Medicamento) -> Bool in
            medDeseleccionado == med
        }
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
