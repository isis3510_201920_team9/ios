//
//  SuscripcionDetailViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/8/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class SuscripcionDetailViewController: UIViewController {
    var suscripcion: Suscripcion?
    
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var diaLabel: UILabel!
    @IBOutlet weak var horaLabel: UILabel!
    @IBOutlet weak var frecuenciaLabel: UILabel!
    @IBOutlet weak var observacionesLabel: UILabel!
    @IBOutlet weak var imageQR: UIImageView!
    
    @IBAction func cancelButton(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let date = suscripcion!.fechaAgendada[0]
        let f = DateFormatter()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        
        nombreLabel.text = suscripcion?.farmacia.nombre
        diaLabel.text = f.weekdaySymbols[calendar.component(.weekday, from: suscripcion!.fechaAgendada[0])-1]
        horaLabel.text = "\(hour):\(minutes)"
        frecuenciaLabel.text = "Cada dos semanas"
        observacionesLabel.text = "No tiene observaciones"
        
        let email = Autenticar.getUser().email!
        let farmacia = suscripcion!.farmacia.nombre
        let medicamentos = suscripcion!.medicamentos.count
        imageQR.image = generateQRCode(from: "https://us-central1-pharmaco-68bb9.cloudfunctions.net/qrSuscripcionIOS?suscripcion=\(suscripcion!.id)&email=\(email)&farmacia=\(farmacia)&medicamentos=\(medicamentos)")
        
        print("https://us-central1-pharmaco-68bb9.cloudfunctions.net/qrSuscripcionIOS?suscripcion=\(suscripcion!.id)&email=\(email)&farmacia=\(farmacia)&medicamentos=\(medicamentos)")
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    
   var label: UILabel?
   
   @objc func showToastNoConn() {
       self.UI {
           self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
       }
   }
   
   @objc func showToastConn() {
       self.UI {
           if self.label != nil {
               self.hideToast(toast: self.label!)
           }
           
           self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
           self.label = nil
       }
   }
   
   override func viewDidAppear(_ animated: Bool) {
       if !LoginViewController.conn! && self.label==nil  {
           self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
       }
   }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "suscripcionMedsDetailSegue" {
            let vc = segue.destination as? SuscripcionDetailTableViewController
            vc!.suscripcion = suscripcion
        }
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
}
