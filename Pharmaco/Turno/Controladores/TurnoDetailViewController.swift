//
//  TurnoDetailViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/5/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit
import MapKit

class TurnoDetailViewController: UIViewController {
    
    weak var turno: Turno?
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var farmaciaLabel: UILabel!
    @IBOutlet weak var fechaLabel: UILabel!
    @IBOutlet weak var observacionesLabel: UILabel!
    @IBOutlet weak var qr: UIImageView!
    
    @IBAction func volverButton(_ sender: UIButton) {
        performSegueToReturnBack()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY hh:mm"
        farmaciaLabel.text = turno!.farmacia.nombre
        fechaLabel.text = formatter.string(from: turno!.fechaAgendada)
        observacionesLabel.text = turno?.observacionesEspecial
        let email = Autenticar.getUser().email!
        let farmacia = turno!.farmacia.nombre
        let medicamentos = turno!.medicamentos.count
        qr.image = generateQRCode(from: "https://us-central1-pharmaco-68bb9.cloudfunctions.net/qrTurnoIOS?turno=\(turno!.id)&email=\(email)&farmacia=\(farmacia)&medicamentos=\(medicamentos)")
        
        print("https://us-central1-pharmaco-68bb9.cloudfunctions.net/qrTurnoIOS?turno=\(turno!.id)&email=\(email)&farmacia=\(farmacia)&medicamentos=\(medicamentos)")
        FarmaciaPersistence.getFarmacias{(list, error) in
            for farmacia in list {
                if farmacia.nombre == self.turno!.farmacia.nombre {
                    self.map.addAnnotation(FarmaciaMarker(farmacia: farmacia))
                }
            }
        }
        
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    
    var label: UILabel?
    
    @objc func showToastNoConn() {
        self.UI {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    @objc func showToastConn() {
        self.UI {
            if self.label != nil {
                self.hideToast(toast: self.label!)
            }
            
            self.label = self.showToast(message: "Conectado nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
            self.label = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !LoginViewController.conn! && self.label==nil  {
            self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is TurnoMedsTableViewController, segue.identifier == "TurnoTableViewSegue" {
            let vc = segue.destination as! TurnoMedsTableViewController
            vc.meds = turno!.medicamentos
        }
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
}
