//
//  TurnoTableViewController.swift
//  Pharmaco
//
//  Created by Admin on 10/5/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class TurnoTableViewController: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    var turnos = [Turno]()
    var turno: Turno?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        TurnoPersistence.getTurnosVigentes(){(lista, error) in
            self.turnos = lista
            self.tableView.reloadData()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadList), name: NSNotification.Name(rawValue: "TurnoListUpdate"), object: nil)

    }
    
    @objc func reloadList() {
        TurnoPersistence.getTurnosVigentes{(lista, error) in
            self.turnos = lista
            self.tableView.reloadData()
        }
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "¡Hola!"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }

    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Parece que no tienes ningún turno agregado"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .body)]
        return NSAttributedString(string: str, attributes: attrs)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return turnos.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "medsTurnoCell", for: indexPath) as? TurnoTableViewCell else {
            fatalError("No se pudo instanciar")
        }
        
        let turno = turnos[indexPath.row]
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd/MM/yyyy hh:MM"
        // Configure the cell...
        cell.nombreLabel.text = "\(turno.medicamentos[0].nombre) + \(turno.medicamentos.count - 1)"
        cell.direccionLabel.text = turno.farmacia.direccion
        cell.fechaLabel.text = formatter.string(from: turno.fechaAgendada) 
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let turnoSeleccionado = turnos[indexPath.row]
        turno = turnoSeleccionado
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let index = tableView.indexPathForSelectedRow{
            turno = turnos[index.row]
            if segue.identifier == "turnoDetailSegue"{
                let vc = segue.destination as? TurnoDetailViewController
                vc?.turno = turno
            }
        }
    }
    
    override func tableView(_ tableView: UITableView,
                            trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        // Write action code for the trash
        let TrashAction = UIContextualAction(style: .normal, title:  "Eliminar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            let alertMenu = UIAlertController(title: "Eliminar", message: "¿Eliminar turno?", preferredStyle: .alert)
            
            let confirmar = UIAlertAction(title: "Confirmar", style: .destructive, handler: {
                (alert: UIAlertAction) -> Void in
                self.turnos.remove(at: indexPath.row)
                self.tableView.reloadData()
            })
            let cancelar = UIAlertAction(title: "Cancelar", style: .default)
            
            alertMenu.addAction(confirmar)
            alertMenu.addAction(cancelar)
            self.present(alertMenu, animated: true, completion: nil)
            success(true)
        })
        TrashAction.backgroundColor = .red
        
        //           // Write action code for the Flag
        //           let FlagAction = UIContextualAction(style: .normal, title:  "Flag", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
        //               print("Update action ...")
        //               success(true)
        //           })
        //           FlagAction.backgroundColor = .orange
        
        // Write action code for the More
//        let MoreAction = UIContextualAction(style: .normal, title:  "Editar", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
//            print("Update action ...")
//            success(true)
//        })
//        MoreAction.backgroundColor = .gray
        
        
        return UISwipeActionsConfiguration(actions: [TrashAction])
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
