//
//  TurnoTableViewCell.swift
//  Pharmaco
//
//  Created by Admin on 10/5/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import UIKit

class TurnoTableViewCell: UITableViewCell {
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var direccionLabel: UILabel!
    @IBOutlet weak var fechaLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
