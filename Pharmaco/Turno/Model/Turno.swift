//
//  Turno.swift
//  Pharmaco
//
//  Created by Admin on 10/3/19.
//  Copyright © 2019 Universidad de Los Andes. All rights reserved.
//

import Foundation

class Turno {
    var id: String
    var especial: Bool
    var observacionesEspecial: String?
    var fechaAgendada: Date
    var QR: String
    var fechaEntrega: Date?
    var exitoso: Bool?
    var novedades: String?
    var farmacia: Farmacia
    var medicamentos: [Medicamento]
    var ordenes: [OrdenMedica]
    
    init(id: String, especial: Bool, observacionesEspecial: String?, fechaAgendada: Date, QR: String, fechaEntrega: Date?, exitoso: Bool?, novedades: String?, farmacia: Farmacia, medicamentos: [Medicamento], ordenes: [OrdenMedica]) {
        self.id = id
        self.especial = especial
        self.observacionesEspecial = observacionesEspecial
        self.fechaAgendada = fechaAgendada
        self.QR = QR
        self.fechaEntrega = fechaEntrega
        self.exitoso = exitoso
        self.novedades = novedades
        self.farmacia = farmacia
        self.medicamentos = medicamentos
        self.ordenes = ordenes
    }
    //constructor para el front
    init(especial: Bool, observacionesEspecial: String?, fechaAgendada: Date, QR: String, farmacia: Farmacia, medicamentos: [Medicamento], ordenes: [OrdenMedica]) {
        self.id = ""
        self.especial = especial
        self.observacionesEspecial = observacionesEspecial
        self.fechaAgendada = fechaAgendada
        self.QR = QR
        self.fechaEntrega = nil
        self.exitoso = nil
        self.novedades = nil
        self.farmacia = farmacia
        self.medicamentos = medicamentos
        self.ordenes = ordenes
    }
}
